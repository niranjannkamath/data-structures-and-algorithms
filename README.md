# Data Structures and Algorithms

Contains exercise solutions for courses on Data Structures and Algorithms. It contains my own implementations of algorithms and data structures.

**Algorithmic Toolbox :**
This folder contains source codes for implementing many algorithms based on greedy strategies, divide and conquer and Dynamic Programming.

**Data Structures :**
This folder contains source code for implementations of various data structures like hash tables, priority queues and BSTs

**Algorithms on Graphs :**
This folder contains source code for algorithms on graphs like BFS, DFS, Dijkstra's algorithm etc.

**Algorithms on Strings :**
This folder contains source code for various string processing algorithms like Knuth-Morris-Pratt algorithm, and implementations of various data structures like tries, suffix tries and suffix trees used in string processing applications. 