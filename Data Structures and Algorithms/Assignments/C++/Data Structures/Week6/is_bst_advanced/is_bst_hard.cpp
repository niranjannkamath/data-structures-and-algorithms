#include <algorithm>
#include <iostream>
#include <vector>
#if defined(__unix__) || defined(__APPLE__)
#include <sys/resource.h>
#endif

using std::cin;
using std::cout;
using std::endl;
using std::vector;

struct Node {
  int key;
  int left;
  int right;

  Node() : key(0), left(-1), right(-1) {}
  Node(int key_, int left_, int right_) : key(key_), left(left_), right(right_) {}
};

bool check_bst(const vector<Node>& tree, int index, int lower, int upper, bool init_left, bool is_left) {
    if(tree.empty() || index == -1) {
        return true;
    }
    else {
        int curr_key = tree.at(index).key;
        int left_index = tree.at(index).left;
        int right_index = tree.at(index).right;
        if(init_left == is_left) {
            if(init_left && curr_key >= upper) {
                return false;
            }
            else if(!init_left && curr_key < lower) {
                return false;
            }
        }
        else {
            if(init_left && (curr_key < lower || curr_key >= upper)) {
                return false;
            }
            else if(!init_left && (curr_key < lower || curr_key >= upper)) {
                return false;
            }
        }
        bool is_left_bst = check_bst(tree, left_index, lower, curr_key, init_left, true);
        bool is_right_bst = check_bst(tree, right_index, curr_key, upper, init_left, false);
        return (is_left_bst && is_right_bst);
    }
}

bool IsBinarySearchTree(const vector<Node>& tree) {
    if(!tree.empty()) {
        bool left = check_bst(tree, tree.at(0).left, tree.at(0).key, tree.at(0).key, true, true);
        bool right = check_bst(tree, tree.at(0).right, tree.at(0).key, tree.at(0).key, false, false);
        return (left && right);
    }
    return true;
}

int main_with_large_stack_space() {
    int nodes;
    cin >> nodes;
    vector<Node> tree;
    for (int i = 0; i < nodes; ++i) {
        int key, left, right;
        cin >> key >> left >> right;
        tree.push_back(Node(key, left, right));
    }
    if (IsBinarySearchTree(tree)) {
        cout << "CORRECT" << endl;
    } else {
        cout << "INCORRECT" << endl;
    }
    return 0;
}

int main (int argc, char **argv) {
    #if defined(__unix__) || defined(__APPLE__)
    // Allow larger stack space
    const rlim_t kStackSize = 16 * 1024 * 1024;   // min stack size = 16 MB
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                std::cerr << "setrlimit returned result = " << result << std::endl;
            }
        }
    }
    #endif

    return main_with_large_stack_space();
}