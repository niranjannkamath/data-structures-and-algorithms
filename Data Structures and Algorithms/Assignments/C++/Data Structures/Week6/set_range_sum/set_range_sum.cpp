#include <iostream>
#include <vector>

using std::vector;
using std::cin;
using std::cout;

class SplayTree {
public:
    struct Vertex {
        int key;
        Vertex* parent;
        Vertex* left;
        Vertex* right;

        Vertex(int key_, Vertex* parent_, Vertex* left_, Vertex* right_) : key(key_), parent(parent_), left(left_), right(right_) {}; 
    };
    Vertex* root = new Vertex(-1, nullptr, nullptr, nullptr);
    Vertex* temp;
    void STAdd(int i) {
        temp = new Vertex(i, nullptr, nullptr, nullptr);
        if(root->key != -1) {
            Vertex* v = STFind(i);
            temp->parent = v;
            if(i < v->key) {
                v->left = temp;
            }
            else if(i > v->key) {
                v->right = temp;
            }
            return;
        }
        else {
            free(root);
            root = new Vertex(i, nullptr, nullptr, nullptr);
            splay_tree.push_back(root);
            return;
        }
    }
    void STDel(int i) {
        Vertex* v = STFind(i);
        if(v->key != i) {
            return;
        }
        splay(next(v));
        splay(v);
        Vertex* l = v->left;
        Vertex* r = v->right;
        free(root);
        copy_vertex(root, r);
        r->left = l;
        l->parent = r;
        v->key = -1;
    }
    Vertex* STFind(int i) {
        if(root->key != -1) {
            Vertex* vert = find(i, root);
            splay(vert);    
            return vert;
        }
        else {
            return nullptr;
        }
    }
    long long sum(int l, int r) {
        Vertex* v = STFind(l);
        if(v->key > r) {
            return 0;
        }
        long long s = v->key;
        while(v->key <= r) {
            v = next(v);
            if(v == nullptr) {
                return s;
            }
            s += v->key;
        }
        return s;
    }
private:
    vector<Vertex*> splay_tree;
    void small_rotation(Vertex* v) {
        if(v->parent != nullptr) {
            Vertex* p = v->parent;
            if(p->left == v) {
                p->left = v->right;
                v->right = p;
            }
            else {
                p->right = v->left;
                v->left = p;
            }
            v->parent = p->parent;
            p->parent = v;
            return;
        }
        else {
            return;
        }
    }
    void big_rotation(Vertex* v) {
        small_rotation(v);
        if(v->parent != nullptr) {
            Vertex* p = v->parent;
            if(v->key < p->key) {
                p->left = v;
            }
            else {
                p->right = v;
            }
            small_rotation(v);
            return;
        }
        else {
            return;
        }
    }
    void splay(Vertex* v) {
        if(v->parent == nullptr) {
            return;
        }
        else {
            if(v->parent->parent != nullptr) {
                big_rotation(v);
            }
            else {
                small_rotation(v);
            }
            splay(v);
        }
    }
    Vertex* find(int i, Vertex* v) {
        if(root->key == -1 || v->key == -1) {
            return nullptr;
        }
        else {
            if(v->key > i) {
                if(v->left == nullptr) {
                    return v;
                }
                return find(i, v->left);
            }
            else if(v->key < i) {
                if(v->right == nullptr) {
                    return v;
                }
                return find(i, v->right);
            }
            else {
                return v;
            }
        }
    }
    Vertex* next(Vertex* vert) {
        Vertex* v = vert;
        if(v->right != nullptr) {
            v = v->right;
            while(v->left != nullptr) {
                v = v->left;
            }
            return v;
        }
        else {
            if(v->parent != nullptr) {
                int k = v->key;
                v = v->parent;
                while(v->key < k) {
                    if(v->parent == nullptr) {
                        return nullptr;
                    }
                    v = v->parent;
                }
            }
            return v;
        }
    }
    void copy_vertex(Vertex* v1, Vertex* v2) {
        v1->key = v2->key;
        v1->parent = v2->parent;
        v1->left = v2->left;
        v1->right = v2->right;
    }
};

const int MODULO = 1000000001;

int main(){
    int n, key, l, r;
    char query;
    cin >> n;
    long long sum = 0, cumul_sum = 0;
    SplayTree st;
    for(int i = 0; i < n; ++i) {
        cin >> query;
        if(query == '+') {
            cin >> key;
            st.STAdd(static_cast<int>((key + cumul_sum) % MODULO));
        }
        else if(query == '-') {
            cin >> key;
            st.STDel(static_cast<int>((key + cumul_sum) % MODULO));
        }
        else if(query == '?') {
            cin >> key;
            SplayTree::Vertex* vertex = st.STFind(static_cast<int>((key + cumul_sum) % MODULO));
            if(vertex == nullptr) {
                cout << "Not found\n";
            }
            else {
                cout << ((vertex->key == (key + cumul_sum) % MODULO) ? "Found\n" : "Not found\n");
            }
        }
        else {
            cin >> l >> r;
            sum = st.sum(static_cast<int>((l + cumul_sum) % MODULO), static_cast<int>((r + cumul_sum) % MODULO));
            cumul_sum += sum;
            cout << sum << '\n';
        }
    }

    return 0;
}