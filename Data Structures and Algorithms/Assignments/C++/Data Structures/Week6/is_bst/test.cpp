#include <algorithm>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

struct Node {
    int key;
    int left;
    int right;

    Node() : key(0), left(-1), right(-1) {}
    Node(int key_, int left_, int right_) : key(key_), left(left_), right(right_) {}
};
bool IsBinarySearchTree(const vector<Node>& tree, int index) {
    int left_index, right_index;
    bool is_left_bst, is_right_bst;
    if(tree.empty() || index == -1) {
        return true;
    }
    else {
        left_index = tree.at(index).left;
        right_index = tree.at(index).right;
        if(left_index != -1 && right_index != -1) {
            if(tree.at(left_index).key >= tree.at(index).key || tree.at(right_index).key <= tree.at(index).key)
        }
        is_left_bst = IsBinarySearchTree(tree, left_index);
        is_right_bst = IsBinarySearchTree(tree, right_index);
    }
    return (is_left_bst && is_right_bst);
}

int main() {
    int nodes;
    cin >> nodes;
    vector<Node> tree;
    for (int i = 0; i < nodes; ++i) {
        int key, left, right;
        cin >> key >> left >> right;
        tree.push_back(Node(key, left, right));
    }
    if (IsBinarySearchTree(tree, 0)) {
        cout << "CORRECT" << endl;
    } else {
        cout << "INCORRECT" << endl;
    }
    return 0;
}