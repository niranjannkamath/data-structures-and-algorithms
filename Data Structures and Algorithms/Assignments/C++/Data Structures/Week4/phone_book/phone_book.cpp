#include <iostream>
#include <vector>
#include <string>

using std::string;
using std::vector;
using std::cin;

struct Query {
    string type, name;
    int number;
};

vector<Query> read_queries() {
    int n;
    cin >> n;
    vector<Query> queries(n);
    for (int i = 0; i < n; ++i) {
        cin >> queries[i].type;
        if (queries[i].type == "add") {
            cin >> queries[i].number >> queries[i].name;
        }
        else {
            cin >> queries[i].number;
        }
    }
    return queries;
}

void write_responses(const vector<string>& result) {
    for (size_t i = 0; i < result.size(); ++i) {
        std::cout << result[i] << "\n";
    }
}

vector<string> process_queries(const vector<Query>& queries) {
    vector<string> result;
    vector<vector<Query>> contacts(10000);
    vector<Query>* list;
    for (size_t i = 0; i < queries.size(); ++i) {
        int num = queries.at(i).number;
        list = &contacts.at(num % 10000);
        if (queries[i].type == "add") {
            bool was_found = false;
            int j = 0;
            while(j < list->size()) {
                if(list->at(j).number == num) {
                    was_found = true;
                    break;
                }
                j++;
            }
            if(was_found) {
                list->at(j).name = queries.at(i).name;
            }
            else {
                list->push_back(queries.at(i));
            }
        } 
        else if (queries[i].type == "del") {
            for(int j = 0; j < list->size(); ++j) {
                if(list->at(j).number == num) {
                    list->at(j).name = "";
                    break;
                }
            }
        } 
        else {
            string response = "not found";
            for(int j = 0; j < list->size(); ++j) {
                if(list->at(j).number == num && list->at(j).name != "") {
                    response = list->at(j).name;
                    break;
                }
            }
        result.push_back(response);
        }
    }
    return result;
}

int main() {
    write_responses(process_queries(read_queries()));
    return 0;
}
