#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using std::string;
using std::vector;
typedef unsigned long long ull;

struct Data {
    string pattern, text;
};

bool are_equal(string s1, string s2) {
    if(s1.length() != s2.length()) {
        return false;
    }
    else {
        int i = 0;
        while(i < s1.length()) {
            if(s1[i] != s2[i]) {
                return false;
            }
            i++;
        }
        return true;
    }
}

Data read_input() {
    Data data;
    std::cin >> data.pattern >> data.text;
    return data;
}

void print_occurrences(const vector<int>& output) {
    for (size_t i = 0; i < output.size(); ++i) {
        std::cout << output[i] << " ";
    }
    std::cout << "\n";
}

vector<int> get_occurrences(const Data& input) {
    const string &s = input.pattern, t = input.text;
    const int t_len = t.length();
    const int s_len = s.length();

    const ull p = 500009;
    const int x = 3;
    string sub;
    string temp = s;
    char* sub_c = &(temp[0]);
    vector<int> ans;

    ull hash_array[t_len - s_len + 1];
    hash_array[t_len - s_len] = static_cast<ull>(t[t_len - 1]);
    ull y = x;
    ull s_hash = static_cast<ull>(s[s_len - 1]);

    for(int i = t_len - 1; i > t_len - s_len; --i) {
        hash_array[t_len - s_len] *= x;
        hash_array[t_len - s_len] %= p; 
        hash_array[t_len - s_len] += static_cast<ull>(t[i - 1]) % p;
        hash_array[t_len - s_len] %= p;

        s_hash *= x;
        s_hash %= p;
        s_hash += static_cast<ull>(s[i - t_len + s_len - 1]) % p;
        s_hash %= p;

        y = (y * x) % p;
    }
 
    for(int i = t_len - s_len - 1; i >= 0; --i) {
        hash_array[i] = (((x * hash_array[i + 1]) % p + (static_cast<ull>(t[i])) % p - (y * static_cast<ull>(t[i + s_len])) % p) + p) % p;
    }

    for(int i = 0; i < t_len - s_len + 1; ++i) {
        if(hash_array[i] != s_hash) {
            continue;
        }
        else {
            int j = i;
            while(j < i + s_len) {
                *(sub_c + j - i) = t[j];
                j++;
            }
            sub.assign(sub_c);
            if(are_equal(s, sub)) {
                ans.push_back(i);
            }
        }
    }
    
    return ans;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    print_occurrences(get_occurrences(read_input()));
    return 0;
}
