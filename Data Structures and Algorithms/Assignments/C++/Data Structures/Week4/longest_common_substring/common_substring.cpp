#include <iostream>
#include <vector>

using std::vector;
using std::cout;
using std::cin;
typedef unsigned long long ull;

struct Answer {
	size_t i, j, len;
};

Answer solve(const string &s, const string &t) {
    const ull primes[3] = {static_cast<ull>(1E+09 + 7), static_cast<ull>(1E+09 + 9), static_cast<ull>(1E+09 + 33)};
    const ull x = static_cast<ull>(100000007);

    vector<vector<vector<ull> > > ht_s(s.length(), vector<vector<ull> >(3));
    vector<vector<vector<ull> > > ht_t(t.length(), vector<vector<ull> >(3));

    for(int i = 0; i < min(s.length(), t.length()); ++i) {
        for(int j = 0; j < 3; ++j) {
            ht_s.at(i).at(j).resize(i + 2);
            ht_t.at(i).at(j).resize(i + 2);

            ht_s.at(i).at(j).at(0) = 0;
            ht_t.at(i).at(j).at(0) = 0;

            vector<ull> &subs_s = ht_s.at(i).at(j);
            vector<ull> &subs_t = ht_t.at(i).at(j);

            for(int k = 1; k < i + 2; ++k) {
                subs_s.at(k) = (x * subs_s.at(k - 1) % primes[j] + static_cast<ull>(s[k - 1]) % primes[j]) % primes[j];
                subs_t.at(k) = (x * subs_t.at(k - 1) % primes[j] + static_cast<ull>(t[k - 1]) % primes[j]) % primes[j];
            } 
        }
    }

    Answer ans;
    for(int i = 0; i < min(s.length(), t.length()); ++i) {
        int j = 0, k = 0, l = 0;
        while(j < 3) {
            ull key = ht_t.at(i).at(j).at(l + i + 1) - ht_t.at(i).at(j).at(l);
            vector<ull> &sub = ht_s.at(i).at(j);
            if(((sub.at(k + i + 1) - sub.at(k)) % primes[j]) == key) {

            }
        }
    }
}

int main() {
	ios_base::sync_with_stdio(false), cin.tie(0);
	string s, t;
	while (cin >> s >> t) {
		auto ans = solve(s, t);
		cout << ans.i << " " << ans.j << " " << ans.len << "\n";
	}
}
