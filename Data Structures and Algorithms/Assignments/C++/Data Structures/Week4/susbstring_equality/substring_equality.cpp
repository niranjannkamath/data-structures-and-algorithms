#include <iostream>
#include <cmath>
#include <vector>
#include <cassert>
#include <climits>
#include <string>

using namespace std;
typedef unsigned long long ull;

class Solver {
	const string &s;
    const int x = 263;

    vector<ull> h1;
    vector<ull> h2;	

    const ull m1 = static_cast<ull>(7 + 1E+09);
    const ull m2 = static_cast<ull>(9 + 1E+09);

public:
	Solver(string s) : s(s) {	

        h1.resize(s.length() + 1);
        h2.resize(s.length() + 1);

        h1.at(0) = 0;
        h2.at(0) = 0;

        for(int i = 1; i < s.length() + 1; ++i) {
            h1.at(i) = ((x * h1.at(i - 1)) % m1 + static_cast<ull>(s[i - 1]) % m1) % m1;
            h2.at(i) = ((x * h2.at(i - 1)) % m2 + static_cast<ull>(s[i - 1]) % m2) % m2;
        }
	}

    ull power(ull a, ull b, ull m) {
        if(b == 0) {
            return 1;
        }
        else if(b % 2 == 0) {
            return ((power(a, b / 2, m) % m) * (power(a, b / 2, m) % m)) % m;
        }
        else {
            return ((power(a, b / 2, m) % m) * (power(a, b / 2, m) % m) * a) % m;
        }
    }

	bool ask(int a, int b, int l) {
		ull y1 = power(x, l, m1);
        ull y2 = power(x, l, m2);

        ull hash_a_1 = ((h1.at(a + l) - (y1 * h1.at(a)) % m1 + m1) % m1 + m1) % m1;
        ull hash_a_2 = ((h2.at(a + l) - (y2 * h2.at(a)) % m2 + m2) % m2 + m2) % m2;

        ull hash_b_1 = ((h1.at(b + l) - (y1 * h1.at(b)) % m1 + m1) % m1 + m1) % m1;
        ull hash_b_2 = ((h2.at(b + l) - (y2 * h2.at(b)) % m2 + m2) % m2 + m2) % m2;

        return (hash_a_1 == hash_b_1 && hash_a_2 == hash_b_2);
	}
};

int main() {
	ios_base::sync_with_stdio(0), cin.tie(0);

	string s;
	int q;
	cin >> s >> q;
	Solver solver(s);
	for (int i = 0; i < q; i++) {
		int a, b, l;
		cin >> a >> b >> l;
		cout << (solver.ask(a, b, l) ? "Yes\n" : "No\n");
	}
}
