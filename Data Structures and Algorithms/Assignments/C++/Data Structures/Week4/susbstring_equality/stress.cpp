#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <climits>

using namespace std;
typedef unsigned long long ull;

class Solver {
	const string &s;
    const ull x = static_cast<ull>(100000007);
    vector<ull> h1;
    vector<ull> h2;	

    const ull m1 = static_cast<ull>(1E+09 + 7);
    const ull m2 = static_cast<ull>(1E+09 + 9);

public:
	Solver(string s) : s(s) {	

        h1.resize(s.length() + 1);
        h2.resize(s.length() + 1);

        h1.at(0) = 0;
        h2.at(0) = 0;

        for(int i = 1; i < s.length() + 1; ++i) {
            assert(x * h1.at(i - 1) + static_cast<ull>(s[i - 1]) < ULLONG_MAX);
            assert(x * h2.at(i - 1) + static_cast<ull>(s[i - 1]) < ULLONG_MAX);
            h1.at(i) = ((x * h1.at(i - 1)) % m1 + static_cast<ull>(s[i - 1]) % m1) % m1;
            h2.at(i) = ((x * h2.at(i - 1)) % m2 + static_cast<ull>(s[i - 1]) % m2) % m2;
        }
	}

	bool ask(int a, int b, int l) {
		ull y1 = 1, y2 = 1;
        int i = 0;
        while(i < l) {
            y1 = (y1 * x) % m1;
            y2 = (y2 * x) % m2;
            i++;
        }

        ull hash_a_1 = ((h1.at(a + l) - (y1 * h1.at(a)) % m1) % m1 + m1) % m1;
        ull hash_a_2 = ((h2.at(a + l) - (y2 * h2.at(a)) % m2) % m2 + m2) % m2;

        ull hash_b_1 = ((h1.at(b + l) - (y1 * h1.at(b)) % m1) % m1 + m1) % m1;
        ull hash_b_2 = ((h2.at(b + l) - (y2 * h2.at(b)) % m2) % m2 + m2) % m2;

        return (hash_a_1 == hash_b_1 && hash_a_2 == hash_b_2);
	}

    bool ask_naive(int a, int b, int l) {
        return (s.substr(a, l) == s.substr(b, l));
    }
};

int main() {
	ios_base::sync_with_stdio(0), cin.tie(0);

    srand((unsigned) time(NULL));

    while(true) {
        string s = "";
        int q = 1 + rand() % 10;
        int len = 1 + rand() % 20;
        for(int i = 0; i < len; ++i) {
            s += static_cast<char>(97 + rand() % 26);
        }
        Solver solver(s);
        for (int i = 0; i < q; i++) {
            int a, b, l;
            l = 1 + rand() % len;
            if(l == len) {
                a = 0;
                b = 0;
            }
            else {
                a = rand() % (len - l);
                b = rand() % (len - l);
            }
            bool sol, naive;
            naive = solver.ask_naive(a, b, l);
            sol = solver.ask(a, b, l);
            if(sol == !naive) {
                cout << s << "\n\n";
                cout << q << "\n\n";
                cout << a << ' ' << b << ' ' << l << "\n\n";
                cout << sol << ' ' << naive << '\n';
                exit(0);
            }
        }
    }
}