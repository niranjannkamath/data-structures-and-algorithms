#include <iostream>
#include <cmath>
#include <climits>
#include <string>

using std::cin;
using std::cout;
using std::string;
typedef unsigned long long ull;

class Solver {
	const string s;
    const ull x = 9;

    ull h1[500001];
    ull h2[500001];
    //ull h3[500001];

    const ull m1 = 100000007;
    const ull m2 = 100000037;
    //const ull m3 = 10000103;

public:
	Solver(string s) : s(s) {	

        h1[0] = 0;
        h2[0] = 0;
        //h3[0] = 0;

        for(int i = 1; i < s.length() + 1; ++i) {
            h1[i] = ((x * h1[i - 1]) % m1 + static_cast<ull>(s[i - 1])) % m1;
            h2[i] = ((x * h2[i - 1]) % m2 + static_cast<ull>(s[i - 1])) % m2;
            //h3[i] = ((x * h3[i - 1]) % m3 + static_cast<ull>(s[i - 1]) % m3) % m3;
        }
	}

    ull power(ull p, ull q, ull m) {
        if(q == 0) {
            return 1;
        }
        else if(q % 2 == 0) {
            return ((power(p, q / 2, m) % m) * (power(p, q / 2, m) % m)) % m;
        }
        else {
            return p * (((power(p, q / 2, m) % m) * (power(p, q / 2, m) % m)) % m);
        }
    }

	bool ask(int a, int b, int l) {
		ull y1 = power(x, l, m1);
        ull y2 = power(x, l, m2);

        ull hash_a_1 = ((h1[a + l] - (y1 * h1[a]) % m1) % m1 + m1) % m1;
        ull hash_a_2 = ((h2[a + l] - (y2 * h2[a]) % m2) % m2 + m2) % m2;
        //ull hash_a_3 = ((h3[a + l] - (y3 * h3[a]) % m3) % m3 + m3) % m3;

        ull hash_b_1 = ((h1[b + l] - (y1 * h1[b]) % m1) % m1 + m1) % m1;
        ull hash_b_2 = ((h2[b + l] - (y2 * h2[b]) % m2) % m2 + m2) % m2;
        //ull hash_b_3 = ((h3[b + l] - (y3 * h3[b]) % m3) % m3 + m3) % m3;

        // for(int i = 0; i < s.length() + 1; ++i) {
        //     cout << h1.at(i) << ' ' << h2.at(i) << '\n';
        // }

        if (hash_a_1 == hash_b_1 && hash_a_2 == hash_b_2) {
            return (s.substr(a, l) == s.substr(b, l));
        }
        else {
            return false;
        }
    }
};

int main() {
	std::ios_base::sync_with_stdio(0), cin.tie(0);

	string s;
	int q;
	cin >> s >> q;
	Solver solver(s);
	for (int i = 0; i < q; i++) {
		int a, b, l;
		cin >> a >> b >> l;
		cout << (solver.ask(a, b, l) ? "Yes\n" : "No\n");
	}
}