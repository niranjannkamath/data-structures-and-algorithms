# python3

import sys

m1 = 10**9 + 7
m2 = 10**9 + 9
h1 = list()
h2 = list()
x = 3

class Solver:
	def __init__(self, s):
		global h1, h2
		self.s = s
		h1 = [None] * (len(s) + 1)
		h2 = [None] * (len(s) + 1)

		h1[0], h2[0] = 0, 0

		for i in range(1, len(s) + 1):
			h1[i] = (x * h1[i - 1] + ord(s[i - 1])) % m1
			h2[i] = (x * h2[i - 1] + ord(s[i - 1])) % m2
	
	def ask(self, a, b, l):
		y1 = x**l % m1
		y2 = x**l % m2

		hash_a_1 = ((h1[a + l] - (y1 * h1[a]) % m1 + m1) % m1 + m1) % m1
		hash_a_2 = ((h2[a + l] - (y2 * h2[a]) % m2 + m2) % m2 + m2) % m2

		hash_b_1 = ((h1[b + l] - (y1 * h1[b]) % m1 + m1) % m1 + m1) % m1
		hash_b_2 = ((h2[b + l] - (y2 * h2[b]) % m2 + m2) % m2 + m2) % m2

		return (hash_a_1 == hash_b_1 and hash_a_2 == hash_b_2)



s = sys.stdin.readline()
q = int(sys.stdin.readline())
solver = Solver(s)
for i in range(q):
	a, b, l = map(int, sys.stdin.readline().split())
	print("Yes" if solver.ask(a, b, l) else "No")
