#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::list;

struct Query {
    string type, s;
    size_t ind;
};

class QueryProcessor {
    int bucket_count;
    // store all strings in one vector
    vector<list<string> > elems;
    size_t hash_func(const string& s) const {
        static const size_t multiplier = 263;
        static const size_t prime = 1000000007;
        unsigned long long hash = 0;
        for (int i = static_cast<int> (s.size()) - 1; i >= 0; --i) {
            hash = (hash * multiplier + s[i]) % prime;
        }
        return hash % bucket_count;
    }

public:
    explicit QueryProcessor(int bucket_count): bucket_count(bucket_count) {elems.resize(bucket_count);}

    Query readQuery() const {
        Query query;
        cin >> query.type;
        if (query.type != "check")
            cin >> query.s;
        else
            cin >> query.ind;
        return query;
    }

    void writeSearchResult(bool was_found) const {
        std::cout << (was_found ? "yes\n" : "no\n");
    }

    void processQuery(const Query& query) {
        list<string>* ls;
        std::list<string>::iterator it;
        if(query.type == "check") {
            ls = &elems.at(query.ind);
            if(ls->empty()) {
                cout << '\n';
            }
            else {
                it = ls->begin();
                while(it != ls->end()) {
                    cout << *it << ' ';
                    it++;
                }
                cout << '\n';
            }
        }
        else if(query.type == "add") {
            size_t hash_value = hash_func(query.s);
            ls = &elems.at(hash_value);
            if(ls->empty()) {
                ls->push_front(query.s);
            }
            else {
                it = ls->begin();
                bool was_found = false;
                while(it != ls->end()) {
                    if(*it == query.s) {
                        was_found = true;
                        break;
                    }
                    it++;
                }
                if(!was_found) {
                    ls->push_front(query.s);
                }
            }
        }
        else if(query.type == "del") {
            size_t hash_value = hash_func(query.s);
            ls = &elems.at(hash_value);
            if(!ls->empty()) {
                it = ls->begin();
                while(it != ls->end()) {
                    if(*it == query.s) {
                        ls->erase(it);
                        break;
                    }
                    it++;
                }
            }
        }
        else {
            size_t hash_value = hash_func(query.s);
            ls = &elems.at(hash_value);
            bool was_found = false;
            if(!ls->empty()) {
                it = ls->begin();
                while(it != ls->end()) {
                    if(*it == query.s) {
                        was_found = true;
                        break;
                    }
                    it++;
                }
            }
            cout << (was_found ? "yes\n" : "no\n");
        }
    }

    void processQueries() {
        int n;
        cin >> n;
        for (int i = 0; i < n; ++i)
            processQuery(readQuery());
    }
};

int main() {
    std::ios_base::sync_with_stdio(false);
    int bucket_count;
    cin >> bucket_count;
    QueryProcessor proc(bucket_count);
    proc.processQueries();
    return 0;
}
