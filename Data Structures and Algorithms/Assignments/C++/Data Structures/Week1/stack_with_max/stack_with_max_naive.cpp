#include <iostream>
#include <vector>
#include <string>
#include <cassert>
#include <algorithm>

using std::cin;
using std::string;
using std::vector;
using std::cout;

class StackWithMax {
    vector<int> stack;
    vector<int> maxs;

  public:

    void Push(int value) {
        if(stack.empty() || value > maxs.at(maxs.size() - 1)) {
            maxs.push_back(value);
        }
        else {
            maxs.push_back(maxs.at(maxs.size() - 1));
        }
        stack.push_back(value);
    }

    void Pop() {
        assert(stack.size());
        maxs.pop_back();
        stack.pop_back();
    }

    int Max() const {
        assert(stack.size());
        return maxs.at(maxs.size() - 1);
    }
};

int main() {
    int num_queries = 0;
    cin >> num_queries;

    string query;
    string value;

    StackWithMax stack;

    for (int i = 0; i < num_queries; ++i) {
        cin >> query;
        if (query == "push") {
            cin >> value;
            stack.Push(std::stoi(value));
        }
        else if (query == "pop") {
            stack.Pop();
        }
        else if (query == "max") {
            cout << stack.Max() << "\n";
        }
        else {
            assert(0);
        }
    }
    return 0;
}