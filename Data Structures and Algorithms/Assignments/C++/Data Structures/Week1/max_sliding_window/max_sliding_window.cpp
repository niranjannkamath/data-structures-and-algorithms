#include <iostream>
#include <cassert>
#include <queue>
#include <stack>
#include <vector>

using std::cin;
using std::cout;
using std::queue;
using std::stack;
using std::vector;
using std::max;

class StackWithMax {
    vector<int> stack;
    vector<int> maxs;

  public:

    void push(int value) {
        if(stack.empty() || value > maxs.at(maxs.size() - 1)) {
            maxs.push_back(value);
        }
        else {
            maxs.push_back(maxs.at(maxs.size() - 1));
        }
        stack.push_back(value);
    }

    void pop() {
        assert(stack.size());
        maxs.pop_back();
        stack.pop_back();
    }

    int max() const {
        assert(stack.size());
        return maxs.at(maxs.size() - 1);
    }

    bool empty() {
        return stack.empty();
    }

    int top() {
        assert(stack.size());
        return stack.at(stack.size() - 1);
    }
};

void max_sliding_window(vector<int> &A, int w) {

    int n = A.size();

    StackWithMax s1;
    StackWithMax s2;

    for(int i = 0; i < w; i++) {
        s1.push(A.at(i));
    }

    std::cout << s1.max() << ' ';

    for(int i = w; i < n; ++i) {
        if(s2.empty()) {
            while(!s1.empty()) {
                s2.push(s1.top());
                s1.pop();
            }
        }
        s2.pop();
        s1.push(A.at(i));
        if(!s2.empty()) {
            cout << max(s1.max(), s2.max()) << ' ';
        }
        else {
            cout << s1.max() << ' ';
        }
    }

    return;
}

int main() {
    int n = 0;
    cin >> n;

    vector<int> A(n);
    for (size_t i = 0; i < n; ++i) {
        cin >> A.at(i);
    }

    int w = 0;
    cin >> w;

    max_sliding_window(A, w);
    cout << '\n';

    return 0;
}