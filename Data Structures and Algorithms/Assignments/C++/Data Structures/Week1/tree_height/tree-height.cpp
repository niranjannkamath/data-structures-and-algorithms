#include <algorithm>
#include <iostream>
#include <vector>
#if defined(__unix__) || defined(__APPLE__)
#include <sys/resource.h>
#endif

class Node;

class Node {
public:
    int key;
    Node *parent;
    std::vector<Node *> children;

    Node() {
        this->parent = NULL;
    }

    void setParent(Node *theParent) {
        parent = theParent;
        parent->children.push_back(this);
    }
};

int get_max_height(Node *v) {

    int maxHeight = 1;
    int height = 0;

    if(v->children.empty()) {
        return maxHeight;
    }
    else {
        for(int i = 0; i < v->children.size(); ++i) {
            height = 1 + get_max_height(v->children.at(i));
            if(height > maxHeight) {
                maxHeight = height;
            }
        }
    }

    return maxHeight;
}

int main_with_large_stack_space() {
    std::ios_base::sync_with_stdio(0);
    int n;
    std::cin >> n;

    std::vector<Node> nodes;
    nodes.resize(n);
    for (int child_index = 0; child_index < n; child_index++) {
        int parent_index;
        std::cin >> parent_index;
        if (parent_index >= 0) {
            nodes[child_index].setParent(&nodes[parent_index]);
        }
        nodes[child_index].key = child_index;
    }
    
    int maxHeight = 0;
    Node *v = &nodes[0];
    while(v->parent != NULL) {
        v = v->parent;
    }

    maxHeight = get_max_height(v);
        
    std::cout << maxHeight << std::endl;
    return 0;
}

int main (int argc, char **argv)
{
#if defined(__unix__) || defined(__APPLE__)
  // Allow larger stack space
  const rlim_t kStackSize = 16 * 1024 * 1024;   // min stack size = 16 MB
  struct rlimit rl;
  int result;

  result = getrlimit(RLIMIT_STACK, &rl);
  if (result == 0)
  {
      if (rl.rlim_cur < kStackSize)
      {
          rl.rlim_cur = kStackSize;
          result = setrlimit(RLIMIT_STACK, &rl);
          if (result != 0)
          {
              std::cerr << "setrlimit returned result = " << result << std::endl;
          }
      }
  }

#endif
  return main_with_large_stack_space();
}
