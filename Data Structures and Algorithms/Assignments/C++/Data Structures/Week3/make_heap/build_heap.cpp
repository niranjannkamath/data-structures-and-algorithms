#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using std::vector;
using std::cin;
using std::cout;
using std::swap;
using std::pair;
using std::make_pair;

class HeapBuilder {
    private:
    vector<int> data_;
    vector< pair<int, int> > swaps_;

    void WriteResponse() const {
        cout << swaps_.size() << "\n";
        for (int i = 0; i < swaps_.size(); ++i) {
        cout << swaps_[i].first << " " << swaps_[i].second << "\n";
        }
    }

    void ReadData() {
        int n;
        cin >> n;
        data_.resize(n);
        for(int i = 0; i < n; ++i)
        cin >> data_[i];
    }

    void SiftDown(int i) {
        if(i > data_.size() / 2 - 1) {
            return;
        }
        else if(2 * i + 2 < data_.size()) {
            if(data_.at(2 * i + 2) < data_.at(2 * i + 1)) {
                if(data_.at(i) > data_.at(2 * i + 2)) {
                    pair<int, int> p;
                    p.first = i;
                    p.second = 2 * i + 2;
                    swaps_.push_back(p);
                    swap(data_.at(i), data_.at(2 * i + 2));
                    SiftDown(2 * i + 2);
                }
            }
            else if(data_.at(i) > data_.at(2 * i + 1)){
                pair<int, int> p;
                p.first = i;
                p.second = 2 * i + 1;
                swaps_.push_back(p);
                swap(data_.at(i), data_.at(2 * i + 1));
                SiftDown(2 * i + 1);
            }
        }
        else if(data_.at(2 * i + 1) < data_.at(i)) {
            pair<int, int> p;
            p.first = i;
            p.second = 2 * i + 1;
            swaps_.push_back(p);
            swap(data_.at(i), data_.at(2 * i + 1));
            SiftDown(2 * i + 1);
        }
        return;
    }

    void GenerateSwaps() {
        swaps_.clear();
        
        for(int i = data_.size() / 2 - 1; i >= 0; --i) {
            SiftDown(i);
        }
    }

    public:
    void Solve() {
        ReadData();
        GenerateSwaps();
        WriteResponse();
    }
};

int main() {
    std::ios_base::sync_with_stdio(false);
    HeapBuilder heap_builder;
    heap_builder.Solve();
    return 0;
}
