#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using std::vector;
using std::cin;
using std::cout;
using std::swap;
using std::min;

class JobQueue {
    private:
    int num_workers_;
    vector<int> jobs_;

    vector<int> assigned_workers_;
    vector<long long> start_times_;

    vector<int> thread_priorities_;
    vector<long long> job_priorities_;

    void WriteResponse() {
        for (int i = 0; i < jobs_.size(); ++i) {
        cout << assigned_workers_[i] << " " << start_times_[i] << "\n";
        }
    }

    void ReadData() {
        int m;
        cin >> num_workers_ >> m;
        jobs_.resize(m);
        job_priorities_.resize(num_workers_);
        for(int i = 0; i < m; ++i) {
            cin >> jobs_[i];
        }
    }

    void AssignJobs() {
        int i = 0, current_thread = 0, p = 0, zero_count = 0;
        while(current_thread < num_workers_ && i < jobs_.size()) {
            start_times_.push_back(0);
            assigned_workers_.push_back(current_thread);
            if(jobs_.at(i) != 0) {
                current_thread++;
            }
            i++;
        }
        while(p < jobs_.size() && zero_count < num_workers_) {
            if(jobs_.at(p) != 0) {
                thread_priorities_.push_back(zero_count);
                job_priorities_.at(zero_count) = jobs_.at(p);
                zero_count++;
            }
            p++;
        }
        if(i >= jobs_.size()) {
            return;
        }
        for(int j = assigned_workers_.size() / 2 - 1; j >= 0; --j) {
            SiftDown(j);
        }
        int next_thread_;
        long long job_time_;
        while(i < jobs_.size()) {
            next_thread_ = GetMinThread();
            job_time_ = GetMinJob();
            ExtractMinThread();
            ExtractMinJob();
            assigned_workers_.push_back(next_thread_);
            thread_priorities_.push_back(next_thread_);
            start_times_.push_back(start_times_.at(next_thread_) + job_time_);
            job_priorities_.push_back(jobs_.at(i) + start_times_.at(start_times_.size() - 1));
            SiftUp(job_priorities_.size() - 1);
            i++;
        }
    }

    void SiftUp(int i) {
        if(i <= 0 || job_priorities_.empty()) {
            return;
        }

        if(i % 2 != 0) {
            if(job_priorities_.at((i - 1) / 2) < job_priorities_.at(i)) {
                return;
            }
            else if(job_priorities_.at((i - 1) / 2) == job_priorities_.at(i)) {
                if(thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i)) {
                    swap(job_priorities_.at((i - 1) / 2), job_priorities_.at(i));
                    swap(thread_priorities_.at((i - 1) / 2), thread_priorities_.at(i));
                }
                else {
                    return;
                }
            }
            else {
                swap(job_priorities_.at(i), job_priorities_.at((i - 1) / 2));
                swap(thread_priorities_.at(i), thread_priorities_.at((i - 1) / 2));
                SiftUp((i - 1) / 2);
            }
        }
        else {
            if(job_priorities_.at(i - 1) == job_priorities_.at(i) && thread_priorities_.at(i - 1) > thread_priorities_.at(i)) {
                swap(job_priorities_.at(i - 1), job_priorities_.at(i));
                swap(thread_priorities_.at(i - 1), thread_priorities_.at(i));
            }

            if(job_priorities_.at((i - 1) / 2) < job_priorities_.at(i)) {
                return;
            }
            else if(job_priorities_.at((i - 1) / 2) == job_priorities_.at(i)) {
                if(thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i - 1) && thread_priorities_.at((i - 1) / 2) < thread_priorities_.at(i)) {
                    swap(job_priorities_.at((i - 1) / 2), job_priorities_.at(i - 1));
                    swap(thread_priorities_.at((i - 1) / 2), thread_priorities_.at(i - 1));
                }
                else if(thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i - 1) && thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i)) {
                    swap(job_priorities_.at((i - 1) / 2), job_priorities_.at(i));
                    swap(thread_priorities_.at((i - 1) / 2), thread_priorities_.at(i));
                }
                else {
                    return;
                }
            }
            else {
                swap(job_priorities_.at(i), job_priorities_.at((i - 1) / 2));
                swap(thread_priorities_.at(i), thread_priorities_.at((i - 1) / 2));
                SiftUp((i - 1) / 2);
                SiftUp(i - 1);
            }
        }
    }
    
    void SiftDown(int i) {
        if(i > job_priorities_.size() / 2 - 1) {
            return;
        }
        else if(job_priorities_.size() == 1) {
            return;
        }
        else if(job_priorities_.empty()) {
            return;
        }
        else if(2 * i + 2 < job_priorities_.size()) {
            if(job_priorities_.at(2 * i + 2) < job_priorities_.at(2 * i + 1)) {
                if(job_priorities_.at(i) > job_priorities_.at(2 * i + 2)) {
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 2));
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 2));
                    SiftDown(2 * i + 2);
                }
                else if(job_priorities_.at(i) == job_priorities_.at(2 * i + 2) && thread_priorities_.at(i) > thread_priorities_.at(2 * i + 2)) {
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 2));
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 2));
                    SiftDown(2 * i + 2);
                }
            }
            else if(job_priorities_.at(2 * i + 2) == job_priorities_.at(2 * i + 1) && thread_priorities_.at(2 * i + 1) > thread_priorities_.at(2 * i + 2)) {
                swap(thread_priorities_.at(2 * i + 1), thread_priorities_.at(2 * i + 2));
                swap(job_priorities_.at(2 * i + 1), job_priorities_.at(2 * i + 2));
                if(job_priorities_.at(i) > job_priorities_.at(2 * i + 1)) {
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                    SiftDown(2 * i + 1);
                }
                else if(job_priorities_.at(i) == job_priorities_.at(2 * i + 1) && thread_priorities_.at(i) > thread_priorities_.at(2 * i + 1)) {
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                    SiftDown(2 * i + 1);
                    if(thread_priorities_.at(2 * i + 1) > thread_priorities_.at(2 * i + 2)) {
                        swap(thread_priorities_.at(2 * i + 1), thread_priorities_.at(2 * i + 2));
                        swap(job_priorities_.at(2 * i + 1), job_priorities_.at(2 * i + 2));
                        SiftDown(2 * i + 2);
                    }
                }
            }
            else if(job_priorities_.at(i) > job_priorities_.at(2 * i + 1)) {
                swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                SiftDown(2 * i + 1);
            }
            else if(job_priorities_.at(i) == job_priorities_.at(2 * i + 1) && thread_priorities_.at(i) > thread_priorities_.at(2 * i + 1)) {
                swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                SiftDown(2 * i + 1);
            }
        }
        else if(job_priorities_.at(i) > job_priorities_.at(2 * i + 1)) {
            swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
            swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
            SiftDown(2 * i + 1);
        }
        return;
    }

    int GetMinThread() {
        return thread_priorities_.at(0);
    }

    void ExtractMinThread() {
        swap(thread_priorities_.at(0), thread_priorities_.at(thread_priorities_.size() - 1));
        thread_priorities_.pop_back();
        SiftDown(0);
    }

    long long GetMinJob() {
        return job_priorities_.at(0);
    }

    void ExtractMinJob() {
        int min_job_ = job_priorities_.at(0);
        swap(job_priorities_.at(0), job_priorities_.at(job_priorities_.size() - 1));
        job_priorities_.pop_back();
        SiftDown(0);
    }

    public:
    void Solve() {
        ReadData();
        AssignJobs();
        WriteResponse();
    }
};

int main() {
    std::ios_base::sync_with_stdio(false);
    JobQueue job_queue;
    job_queue.Solve();
    return 0;
}