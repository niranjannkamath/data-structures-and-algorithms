#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using std::vector;
using std::cin;
using std::cout;
using std::swap;
using std::min;

class JobQueue {
    private:
    int num_workers_;
    vector<int> jobs_;

    vector<int> assigned_workers_;
    vector<long long> start_times_;

    vector<int> assigned_workers_naive_;
    vector<long long> start_times_naive_;

    vector<int> thread_priorities_;
    vector<long long> job_priorities_;

    int randNumber(long long x, int m) {
        x = 3245621 * x + 1568734;
        x = x % 10000019;
        return x % m;
    }

    void WriteResponse() const {
        for (int i = 0; i < jobs_.size(); ++i) {
        cout << assigned_workers_[i] << " " << start_times_[i] << "\n";
        }
    }

    void ReadData() {
        int m;
        num_workers_ = 1 + randNumber(rand() % 100, 100000);
        m = 1 + randNumber(rand() % 100, 100000);
        jobs_.resize(m);
        job_priorities_.resize(num_workers_);
        for(int i = 0; i < m; ++i) {
            jobs_[i] = rand() % static_cast<int>(1E+09);
        }
    }

    void AssignJobs() {
        int i = 0, current_thread = 0, p = 0, zero_count = 0;
        while(current_thread < num_workers_ && i < jobs_.size()) {
            start_times_.push_back(0);
            assigned_workers_.push_back(current_thread);
            if(jobs_.at(i) != 0) {
                current_thread++;
            }
            i++;
        }
        while(p < jobs_.size() && zero_count < num_workers_) {
            if(jobs_.at(p) != 0) {
                thread_priorities_.push_back(zero_count);
                job_priorities_.at(zero_count) = jobs_.at(p);
                zero_count++;
            }
            p++;
        }
        if(i >= jobs_.size()) {
            return;
        }
        thread_priorities_ = assigned_workers_;
        for(int j = assigned_workers_.size() / 2 - 1; j >= 0; --j) {
            SiftDown(j);
        }
        int next_thread_;
        long long job_time_;
        while(i < jobs_.size()) {
            next_thread_ = GetMinThread();
            job_time_ = GetMinJob(); 
            ExtractMinThread();
            ExtractMinJob();
            assigned_workers_.push_back(next_thread_);
            thread_priorities_.push_back(next_thread_);
            start_times_.push_back(start_times_.at(next_thread_) + job_time_);
            job_priorities_.push_back(jobs_.at(i) + start_times_.at(start_times_.size() - 1));
            SiftUp(job_priorities_.size() - 1);
            i++;
        }
    }

    void AssignJobsNaive() {
        // TODO: replace this code with a faster algorithm.
        assigned_workers_naive_.resize(jobs_.size());
        start_times_naive_.resize(jobs_.size());
        vector<long long> next_free_time(num_workers_, 0);
        for (int i = 0; i < jobs_.size(); ++i) {
        int duration = jobs_[i];
        int next_worker = 0;
        for (int j = 0; j < num_workers_; ++j) {
            if (next_free_time[j] < next_free_time[next_worker])
            next_worker = j;
        }
        assigned_workers_naive_[i] = next_worker;
        start_times_naive_[i] = next_free_time[next_worker];
        next_free_time[next_worker] += duration;
        }
    }

    void CheckSolution() {
        int i = 0;
        bool wrong = false;
        while(i < jobs_.size()) {
            if(assigned_workers_.at(i) == assigned_workers_naive_.at(i) && start_times_.at(i) == start_times_naive_.at(i)) {
                i++;
            }
            else {
                wrong = true;
                break;
            }
        }
        if(wrong) {
            cout << num_workers_ << ' ' << jobs_.size() << '\n' << '\n';
            for(int j = 0; j < jobs_.size() ; ++j) {
                cout << jobs_.at(j) << '\n';
            }
            cout << "\n\n";
            cout << assigned_workers_naive_.at(i) << ' ' << start_times_naive_.at(i);
            cout << "\n\n";
            for(int j = 0; j < jobs_.size(); ++j) {
                cout << assigned_workers_.at(j) << ' ' << start_times_.at(j) << '\n';
            }
            cout << "\n\n";
            for(int j = 0; j < jobs_.size(); ++j) {
                cout << assigned_workers_naive_.at(j) << ' ' << start_times_naive_.at(j) << '\n';
            }
            exit(0);
        }
        jobs_.clear();
        job_priorities_.clear();
        start_times_naive_.clear();
        start_times_.clear();
        assigned_workers_naive_.clear();
        assigned_workers_.clear();
        thread_priorities_.clear();
    }

    void SiftUp(int i) {
        if(i <= 0 || job_priorities_.empty()) {
            return;
        }

        if(i % 2 != 0) {
            if(job_priorities_.at((i - 1) / 2) < job_priorities_.at(i)) {
                return;
            }
            else if(job_priorities_.at((i - 1) / 2) == job_priorities_.at(i)) {
                if(thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i)) {
                    swap(job_priorities_.at((i - 1) / 2), job_priorities_.at(i));
                    swap(thread_priorities_.at((i - 1) / 2), thread_priorities_.at(i));
                }
                else {
                    return;
                }
            }
            else {
                swap(job_priorities_.at(i), job_priorities_.at((i - 1) / 2));
                swap(thread_priorities_.at(i), thread_priorities_.at((i - 1) / 2));
                SiftUp((i - 1) / 2);
            }
        }
        else {
            if(job_priorities_.at(i - 1) == job_priorities_.at(i) && thread_priorities_.at(i - 1) > thread_priorities_.at(i)) {
                swap(job_priorities_.at(i - 1), job_priorities_.at(i));
                swap(thread_priorities_.at(i - 1), thread_priorities_.at(i));
            }

            if(job_priorities_.at((i - 1) / 2) < job_priorities_.at(i)) {
                return;
            }
            else if(job_priorities_.at((i - 1) / 2) == job_priorities_.at(i)) {
                if(thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i - 1) && thread_priorities_.at((i - 1) / 2) < thread_priorities_.at(i)) {
                    swap(job_priorities_.at((i - 1) / 2), job_priorities_.at(i - 1));
                    swap(thread_priorities_.at((i - 1) / 2), thread_priorities_.at(i - 1));
                }
                else if(thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i - 1) && thread_priorities_.at((i - 1) / 2) > thread_priorities_.at(i)) {
                    swap(job_priorities_.at((i - 1) / 2), job_priorities_.at(i));
                    swap(thread_priorities_.at((i - 1) / 2), thread_priorities_.at(i));
                }
                else {
                    return;
                }
            }
            else {
                swap(job_priorities_.at(i), job_priorities_.at((i - 1) / 2));
                swap(thread_priorities_.at(i), thread_priorities_.at((i - 1) / 2));
                SiftUp((i - 1) / 2);
                SiftUp(i - 1);
            }
        }
    }
    
    void SiftDown(int i) {
        if(i > job_priorities_.size() / 2 - 1) {
            return;
        }
        else if(job_priorities_.size() == 1) {
            return;
        }
        else if(job_priorities_.empty()) {
            return;
        }
        else if(2 * i + 2 < job_priorities_.size()) {
            if(job_priorities_.at(2 * i + 2) < job_priorities_.at(2 * i + 1)) {
                if(job_priorities_.at(i) > job_priorities_.at(2 * i + 2)) {
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 2));
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 2));
                    SiftDown(2 * i + 2);
                }
                else if(job_priorities_.at(i) == job_priorities_.at(2 * i + 2) && thread_priorities_.at(i) > thread_priorities_.at(2 * i + 2)) {
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 2));
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 2));
                    SiftDown(2 * i + 2);
                }
            }
            else if(job_priorities_.at(2 * i + 2) == job_priorities_.at(2 * i + 1) && thread_priorities_.at(2 * i + 1) > thread_priorities_.at(2 * i + 2)) {
                swap(thread_priorities_.at(2 * i + 1), thread_priorities_.at(2 * i + 2));
                swap(job_priorities_.at(2 * i + 1), job_priorities_.at(2 * i + 2));
                if(job_priorities_.at(i) > job_priorities_.at(2 * i + 1)) {
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                    SiftDown(2 * i + 1);
                }
                else if(job_priorities_.at(i) == job_priorities_.at(2 * i + 1) && thread_priorities_.at(i) > thread_priorities_.at(2 * i + 1)) {
                    swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                    swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                    SiftDown(2 * i + 1);
                    if(thread_priorities_.at(2 * i + 1) > thread_priorities_.at(2 * i + 2)) {
                        swap(thread_priorities_.at(2 * i + 1), thread_priorities_.at(2 * i + 2));
                        swap(job_priorities_.at(2 * i + 1), job_priorities_.at(2 * i + 2));
                        SiftDown(2 * i + 2);
                    }
                }
            }
            else if(job_priorities_.at(i) > job_priorities_.at(2 * i + 1)) {
                swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                SiftDown(2 * i + 1);
            }
            else if(job_priorities_.at(i) == job_priorities_.at(2 * i + 1) && thread_priorities_.at(i) > thread_priorities_.at(2 * i + 1)) {
                swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
                swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
                SiftDown(2 * i + 1);
            }
        }
        else if(job_priorities_.at(i) > job_priorities_.at(2 * i + 1)) {
            swap(thread_priorities_.at(i), thread_priorities_.at(2 * i + 1));
            swap(job_priorities_.at(i), job_priorities_.at(2 * i + 1));
            SiftDown(2 * i + 1);
        }
        return;
    }

    int GetMinThread() {
        return thread_priorities_.at(0);
    }

    void ExtractMinThread() {
        swap(thread_priorities_.at(0), thread_priorities_.at(thread_priorities_.size() - 1));
        thread_priorities_.pop_back();
        SiftDown(0);
    }

    long long GetMinJob() {
        return job_priorities_.at(0);
    }

    void ExtractMinJob() {
        int min_job_ = job_priorities_.at(0);
        swap(job_priorities_.at(0), job_priorities_.at(job_priorities_.size() - 1));
        job_priorities_.pop_back();
        SiftDown(0);
    }

    public:
    void Solve() {
        ReadData();
        AssignJobs();
        AssignJobsNaive();
        CheckSolution();
        //WriteResponse();
    }
};

int main() {
    std::ios_base::sync_with_stdio(false);
    JobQueue job_queue;
    srand((unsigned) time(NULL));
    while(true) {
        job_queue.Solve();
    }
    return 0;
}