#include <iostream>
#include <vector>
#include <queue>
#include <cstdlib>
#include <ctime>

using std::vector;
using std::queue;

int bipartite(vector<vector<int> > &adj, vector<int> &colors, vector<int> &layers, int x) {    
    queue<int> nodes;
    nodes.push(x);
    colors.at(x) = 0;
    layers.at(x) = 0;
    while(!nodes.empty()) {
        int current_node = nodes.front();
        for(int j = 0; j < adj.at(current_node).size(); ++j) {
            if(layers.at(adj.at(current_node).at(j)) == -1) {
                nodes.push(adj.at(current_node).at(j));
                layers.at(adj.at(current_node).at(j)) = layers.at(current_node) + 1;
                colors.at(adj.at(current_node).at(j)) = 1 - colors.at(current_node);
            }
            else if(colors.at(adj.at(current_node).at(j)) != 1 - colors.at(current_node)) {
                return 0;
            }
        }
        nodes.pop();
    }
    for(int i = 0; i < adj.size(); ++i) {
        if(colors.at(i) != layers.at(i) % 2) {
            return 0;
        }
    }
    return 1;
}

int main() {
    srand((unsigned)time(NULL));
    int n, m;
    while(true) {
        n = 1 + rand() % 100000;
        m = rand() % 100000;
        std::cout << n << ' ' << m << '\n';
        vector<vector<int> > adj(n, vector<int>());
        vector<int> colors(n, -1), layers(n, -1);
        for (int i = 0; i < m; i++) {
            int x, y;
            x = 1 + rand() % n;
            y = 1 + rand() % n;
            adj[x - 1].push_back(y - 1);
            adj[y - 1].push_back(x - 1);
            std::cout << x << ' ' << y << '\n';
        }
        std::cout << bipartite(adj, colors, layers, 0) << "\n\n";
    }
}
