#include <iostream>
#include <vector>
#include <queue>

using std::vector;
using std::queue;

int bipartite(vector<vector<int> > &adj, vector<int> &colors, vector<int> &layers, int x) {    
    if(layers.at(x) != -1) {
        return 1;
    }
    queue<int> nodes;
    nodes.push(x);
    colors.at(x) = 0;
    layers.at(x) = 0;
    while(!nodes.empty()) {
        int current_node = nodes.front();
        for(int j = 0; j < adj.at(current_node).size(); ++j) {
            if(layers.at(adj.at(current_node).at(j)) == -1) {
                nodes.push(adj.at(current_node).at(j));
                layers.at(adj.at(current_node).at(j)) = layers.at(current_node) + 1;
                colors.at(adj.at(current_node).at(j)) = 1 - colors.at(current_node);
            }
            else if(colors.at(adj.at(current_node).at(j)) != 1 - colors.at(current_node)) {
                return 0;
            }
        }
        nodes.pop();
    }
    return 1;
}

int main() {
    int n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    vector<int> colors(n, -1), layers(n, -1);
    for (int i = 0; i < m; i++) {
        int x, y;
        std::cin >> x >> y;
        adj[x - 1].push_back(y - 1);
        adj[y - 1].push_back(x - 1);
    }
    int is_bipartite = 0;
    int i = 0, j = 0;
    for(int i = 0; i < n; ++i) {
        is_bipartite = bipartite(adj, colors, layers, i);
        if(!is_bipartite) {
            break; 
        }
    }
    std::cout << is_bipartite << '\n';
}
