#include <iostream>
#include <vector>
#include <queue>

using std::vector;
using std::queue;

int distance(vector<vector<int> > &adj, int s, int t) {
    queue<int> nodes;
    vector<int> layers(adj.size(), -1);
    nodes.push(s);
    layers.at(s) = 0;
    while(!nodes.empty()) {
        int current_node = nodes.front();
        for(int i = 0; i < adj.at(current_node).size(); ++i) {
            if(layers.at(adj.at(current_node).at(i)) == -1) {
                nodes.push(adj.at(current_node).at(i));
                layers.at(adj.at(current_node).at(i)) = layers.at(current_node) + 1;
            }
        }
        nodes.pop();
    }

    return layers.at(t);
}

int main() {
    int n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    for (int i = 0; i < m; i++) {
        int x, y;
        std::cin >> x >> y;
        adj[x - 1].push_back(y - 1);
        adj[y - 1].push_back(x - 1);
    }
    int s, t;
    std::cin >> s >> t;
    s--, t--;
    std::cout << distance(adj, s, t) << '\n';
}
