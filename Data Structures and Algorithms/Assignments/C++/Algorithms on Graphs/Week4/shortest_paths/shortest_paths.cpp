#include <iostream>
#include <limits>
#include <vector>
#include <queue>

using std::vector;
using std::queue;

void bfs(vector<vector<int> > &adj, vector<int> &shortest, queue<int> are_infinite) {
    int current_node;
    while(!are_infinite.empty()) {
        current_node = are_infinite.front();
        for(int i = 0; i < adj.at(current_node).size(); ++i) {
            if(shortest.at(adj.at(current_node).at(i))) {
                shortest.at(adj.at(current_node).at(i)) = 0;
                are_infinite.push(adj.at(current_node).at(i));
            }
        }
        are_infinite.pop();
    }
}

void shortest_paths(vector<vector<int> > &adj, vector<vector<int> > &cost, int s, vector<long long> &distance, vector<int> &reachable, vector<int> &shortest) {
    vector<int> prev(adj.size(), -1);
    int count = adj.size(), current_node, is_relaxed;
    distance.at(s) = 0;
    queue<int> bfs_queue;
    queue<int> are_infinite;
    while(count > 0) {
        bfs_queue.push(s);
        is_relaxed = 0;
        reachable = vector<int>(adj.size(), 0);
        while(!bfs_queue.empty()) {
            current_node = bfs_queue.front();
            reachable.at(current_node) = 1;
            for(int j = 0; j < adj.at(current_node).size(); ++j) {
                if(!reachable.at(adj.at(current_node).at(j))) {
                    bfs_queue.push(adj.at(current_node).at(j));
                    reachable.at(adj.at(current_node).at(j)) = 1;
                }
                if(distance.at(adj.at(current_node).at(j)) > distance.at(current_node) + cost.at(current_node).at(j)) {
                    distance.at(adj.at(current_node).at(j)) = distance.at(current_node) + cost.at(current_node).at(j);
                    prev.at(adj.at(current_node).at(j)) = current_node;
                    is_relaxed = 1;
                    if(count == 1) {
                        shortest.at(adj.at(current_node).at(j)) = 0;
                        are_infinite.push(adj.at(current_node).at(j));
                    }
                } 
            }
            bfs_queue.pop();
        }
        if(!is_relaxed) {
            break;
        }
        count--;
    }

    bfs(adj, shortest, are_infinite);
}

int main() {
    int n, m, s;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    vector<vector<int> > cost(n, vector<int>());
    for (int i = 0; i < m; i++) {
        int x, y, w;
        std::cin >> x >> y >> w;
        adj[x - 1].push_back(y - 1);
        cost[x - 1].push_back(w);
    }
    std::cin >> s;
    s--;
    vector<long long> distance(n, std::numeric_limits<long long>::max());
    vector<int> reachable(n, 0);
    vector<int> shortest(n, 1);
    shortest_paths(adj, cost, s, distance, reachable, shortest);
    for (int i = 0; i < n; i++) {
        if (!reachable[i]) {
        std::cout << "*\n";
        } else if (!shortest[i]) {
        std::cout << "-\n";
        } else {
        std::cout << distance[i] << "\n";
        }
    }
}
