#include <iostream>
#include <vector>

using std::vector;

int negative_cycle(vector<vector<int> > &adj, vector<vector<int> > &cost) {
    int is_relaxed, count = adj.size();
    vector<int> dist(adj.size(), INT32_MAX);
    while(count > 0) {
        is_relaxed = 0;
        for(int i = 0; i < adj.size(); ++i) {
            if(dist.at(i) == INT32_MAX) {
                dist.at(i) = 0;
            }
            for(int j = 0; j < adj.at(i).size(); ++j) {
                if(dist.at(adj.at(i).at(j)) > dist.at(i) + cost.at(i).at(j)) {
                    dist.at(adj.at(i).at(j)) = dist.at(i) + cost.at(i).at(j);
                    is_relaxed = 1;
                }
            }
        }
        count--;
    }
    return is_relaxed;
}

int main() {
    int n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    vector<vector<int> > cost(n, vector<int>());
    for (int i = 0; i < m; i++) {
        int x, y, w;
        std::cin >> x >> y >> w;
        adj[x - 1].push_back(y - 1);
        cost[x - 1].push_back(w);
    }
    std::cout << negative_cycle(adj, cost) << '\n';
}