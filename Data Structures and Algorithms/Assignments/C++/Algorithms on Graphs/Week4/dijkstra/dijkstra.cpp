#include <iostream>
#include <vector>
#include <queue>
#include <climits>
#include <algorithm>

using std::vector;
using std::queue;
using std::pair;
using std::priority_queue;

template<class T, class S, class C>
class priority_queue_modified : public priority_queue<T, S, C> {
    private:
    C comp;
    public:
    priority_queue_modified() : priority_queue<T, S, C>() {}
    template<class InputIt>
    priority_queue_modified(InputIt first, InputIt last) : priority_queue<T, S, C>(first, last) {}
    void extract_min() {
        std::pop_heap(this->c.begin(), this->c.end(), comp);
        this->c.pop_back();
    }
};

class Compare {
public:
    bool operator() (pair<int, int> &p1, pair<int, int> &p2) {
        return p1.second > p2.second;
    }
};

int distance(vector<vector<int> > &adj, vector<vector<int> > &cost, int s, int t) {
    vector<bool> is_extracted(adj.size(), false);
    vector<pair<int, int> > dist(adj.size());
    for(int i = 0; i < adj.size(); ++i) {
        dist.at(i).first = i;
        dist.at(i).second = INT32_MAX;
    }
    dist.at(s).second = 0;
    int current_node;
    vector<int> adj_list;
    priority_queue_modified<pair<int, int>, vector<pair<int, int> >, Compare> dist_queue;
    dist_queue.push(dist.at(s));
    while(!dist_queue.empty()) {
        current_node = dist_queue.top().first;
        if(!is_extracted.at(current_node)) {
            is_extracted.at(current_node) = true;
            adj_list = adj.at(current_node);
            for(int i = 0; i < adj_list.size(); ++i) {
                if(!is_extracted.at(adj_list.at(i)) && dist.at(adj_list.at(i)).second > dist.at(current_node).second + cost.at(current_node).at(i)) {
                    dist.at(adj_list.at(i)).second = dist.at(current_node).second + cost.at(current_node).at(i);
                    pair<int, int> p;
                    p.first = dist.at(adj_list.at(i)).first;
                    p.second = dist.at(adj_list.at(i)).second;
                    dist_queue.push(p);
                }
            }
        }
        dist_queue.extract_min();
    }
    if(dist.at(t).second != INT32_MAX) {
        return dist.at(t).second;
    }
    return -1;
}

int main() {
    int n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    vector<vector<int> > cost(n, vector<int>());
    for (int i = 0; i < m; i++) {
        int x, y, w;
        std::cin >> x >> y >> w;
        adj[x - 1].push_back(y - 1);
        cost[x - 1].push_back(w);
    }
    int s, t;
    std::cin >> s >> t;
    s--, t--;
    std::cout << distance(adj, cost, s, t) << '\n';
}
