#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <vector>
#include <cmath>

using std::vector;
using std::pair;

struct Edge {
    int start, end;
    double distance = 0;
    vector<int> *x, *y;

    Edge() {}
    Edge(int start_, int end_, vector<int> &x_, vector<int> &y_) : start(start_), end(end_), x(&x_), y(&y_) {
        distance = pow((pow(x->at(start) - x->at(end), 2) + pow(y->at(start) - y->at(end), 2)), 0.5);
    };
};

class Compare {
public:
    bool operator()(Edge &e1, Edge &e2) {
        return (e1.distance < e2.distance);
    }
};

class DisjointSets {

private:
    struct Set {
        int parent, height;

        Set(int parent_, int height_) : parent(parent_), height(height_) {}
    };
    vector<Set> sets;
public:
    void make_set(int x) {
        Set s(x, 1);
        sets.push_back(s);
    }
    int find(int i) {
        if(sets.at(i).parent != i) {
            sets.at(i).parent = find(sets.at(i).parent);
        }
        return sets.at(i).parent;
    }
    void Union(int x, int y) {
        int first = find(x);
        int second = find(y);
        if(sets.at(first).height >= sets.at(second).height) {
            sets.at(second).parent = first;
            if(sets.at(first).height == sets.at(second).height) {
                sets.at(first).height += 1;
            }
        }
        else {
            sets.at(first).parent = second;
        }
    }
};

double clustering(vector<int> x, vector<int> y, int k) {
    if(k <= x.size()) {
        int clusters = x.size();
        DisjointSets dis;

        for(int i = 0; i < x.size(); ++i) {
            dis.make_set(i);
        }
        vector<Edge> edges;
        for(int i = 0; i < x.size() - 1; ++i) {
            for(int j = i + 1; j < x.size(); ++j) {
                edges.push_back(Edge(i, j, x, y));  
            }
        }
        Compare comp;
        std::sort(edges.begin(), edges.end(), comp);
        // for(int i = 0; i < edges.size(); ++i) {
        //     std::cout << edges.at(i).distance << ' ';
        // }
        // std::cout << '\n';
        int i = 0;
        Edge current_edge;
        while(clusters > k && i < edges.size()) {
            current_edge = edges.at(i);
            if(dis.find(current_edge.start) != dis.find(current_edge.end)) {
                dis.Union(current_edge.start, current_edge.end);
                clusters--;
            }
            i++;
        }
        if(clusters == k) {
            current_edge = edges.at(i);
            while(dis.find(current_edge.start) == dis.find(current_edge.end) && i < edges.size()) {
                i++;
                current_edge = edges.at(i);
            }
            return current_edge.distance;
        }
        else {
            return -1;
        }
    }
    return -1.;
} 

int main() {
    size_t n;
    int k;
    std::cin >> n;
    vector<int> x(n), y(n);
    for (size_t i = 0; i < n; i++) {
        std::cin >> x[i] >> y[i];
    }
    std::cin >> k;
    std::cout << std::setprecision(10) << clustering(x, y, k) << std::endl;
}