#include <algorithm>
#include <iostream>
#include <queue>
#include <iomanip>
#include <vector>
#include <cmath>
#include <climits>

using std::vector;
using std::pair;
using std::priority_queue;

class Compare {
public:
    bool operator() (pair<int, double> &p1, pair<int, double> &p2) {
        return (p1.second > p2.second);
    }
};

double minimum_distance(vector<int> x, vector<int> y) {
    double result = 0.;
    vector<int> queued(x.size(), 1);
    vector<vector<double> > distance(x.size(), vector<double>(x.size()));
    for(int i = 0; i < x.size(); ++i) {
        for(int j = 0; j < x.size(); ++j) {
            distance.at(i).at(j) = pow((pow(x.at(i) - x.at(j), 2) + pow(y.at(i) - y.at(j), 2)), 0.5);
        }
    }
    vector<pair<int, double> > dist(x.size());
    pair<int, double> p, top_pair;
    for(int i = 0; i < x.size(); ++i) {
        dist.at(i).first = i;
        dist.at(i).second = __DBL_MAX__;
    }
    dist.at(0).second = 0;
    priority_queue<pair<int, double>, vector<pair<int, double> >, Compare> dist_queue;
    for(int i = 0; i < x.size(); ++i) {
        dist_queue.push(dist.at(i));
    }
    int current_node;
    while(!dist_queue.empty()) {
        current_node = dist_queue.top().first;
        if(queued.at(current_node)) {
            result += dist_queue.top().second;
            dist_queue.pop();
            //std::cout << current_node << ' ' << result << '\n';
            queued.at(current_node) = 0;
            for(int i = 0; i < x.size(); ++i) {
                if(queued.at(i) && dist.at(i).second > distance.at(current_node).at(i)) {
                    dist.at(i).second = distance.at(current_node).at(i);
                    p.first = dist.at(i).first;
                    p.second = dist.at(i).second;
                    dist_queue.push(p);
                }
            }
        }
        else {
            dist_queue.pop();
        }
    }

    return result;
} 

int main() {
    size_t n;
    std::cin >> n;
    vector<int> x(n), y(n);
    for (size_t i = 0; i < n; i++) {
        std::cin >> x[i] >> y[i];
    }
    std::cout << std::setprecision(10) << minimum_distance(x, y) << std::endl;
}