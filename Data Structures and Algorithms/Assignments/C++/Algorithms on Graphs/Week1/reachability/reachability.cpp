#include <iostream>
#include <vector>

using std::vector;
using std::pair;


int reach(vector<vector<int> > &adj, int x, int y,  vector<bool>& is_visited) {

    // vector<bool> is_visited(adj.size(), false);
    // is_visited.at(x) = true;
    // int temp = x;
    int is_reachable = 0;
    is_visited.at(x) = true;
    if(x == y) {
        return 1;
    }
    else {
        for(int i = 0; i < adj.at(x).size(); ++i) {
            if(is_visited.at(adj.at(x).at(i))) {
                continue;
            }
            is_reachable = reach(adj, adj.at(x).at(i), y, is_visited);
            if(is_reachable) {
                return 1;
            }
        }
    }
    // for(int i = 0; i < adj.at(x).size(); ++i) {
    //     int j = 0;
    //     int last_visited = 0;
    //     do {
    //         if(temp == y) {
    //             return 1;
    //         }
    //         if(is_visited.at(adj.at(temp).at(j))) {
    //             j++;
    //             last_visited = j;
    //         }
    //         else {
    //             is_visited.at(adj.at(temp).at(j)) = true;
    //             temp = adj.at(temp).at(j);
    //             j = 0;
    //         }
    //     } while(j < adj.at(temp).size())
    // }

    return 0;
}

int main() {
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    vector<bool> is_visited(n, false);
    for (size_t i = 0; i < m; i++) {
        int x, y;
        std::cin >> x >> y;
        adj[x - 1].push_back(y - 1);
        adj[y - 1].push_back(x - 1);
    }
    int x, y;
    std::cin >> x >> y;
    std::cout << reach(adj, x - 1, y - 1, is_visited) << '\n';
}