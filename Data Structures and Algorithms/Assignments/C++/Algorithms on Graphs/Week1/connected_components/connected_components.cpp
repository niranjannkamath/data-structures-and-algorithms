#include <iostream>
#include <vector>

using std::vector;
using std::pair;

void explore(int x, vector<bool>& is_visited, vector<vector<int> >& adj) {
    vector<int> nbrs = adj.at(x);
    is_visited.at(x) = true;
    for(int i = 0; i < nbrs.size(); ++i) {
        if(!is_visited.at(nbrs.at(i))) {
            explore(nbrs.at(i), is_visited, adj);
        }
    }
}   

int number_of_components(vector<vector<int> > &adj) {
    int res = 0;
    vector<bool> is_visited(adj.size(), false);
    for(int i = 0; i < adj.size(); ++i) {
        if(!is_visited.at(i)) {
            explore(i, is_visited, adj);
            res++;
        }
    }
    return res;
}

int main() {
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    for (size_t i = 0; i < m; i++) {
        int x, y;
        std::cin >> x >> y;
        adj[x - 1].push_back(y - 1);
        adj[y - 1].push_back(x - 1);
    }
    std::cout << number_of_components(adj) << '\n';
}
