#include <iostream>
#include <vector>

using std::vector;
using std::pair;

int check_cyclicity(vector<vector<int> > &adj, int x, vector<bool> &is_visited) {
    int is_acyclic = 0;
    is_visited.at(x) = true;
    for(int j = 0; j < adj.at(x).size(); ++j) {
        if(is_visited.at(adj.at(x).at(j))) {
            return 1;
        }
        else{
            is_acyclic = check_cyclicity(adj, adj.at(x).at(j), is_visited);
        }
        if(is_acyclic) {
            return 1;
        }   
    }
    is_visited.at(x) = false;

    return is_acyclic;
}

int acyclic(vector<vector<int> > &adj) {
    int is_acyclic = 0;
    vector<bool> is_visited(adj.size(), false);

    for(int i = 0; i < adj.size(); ++i) {
        is_acyclic = check_cyclicity(adj, i, is_visited);
        if(is_acyclic) {
            return 1;
        }
    }
    return 0;
}

int main() {
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    for (size_t i = 0; i < m; i++) {
        int x, y;
        std::cin >> x >> y;
        adj[x - 1].push_back(y - 1);
    }
    std::cout << acyclic(adj) << '\n';
}
