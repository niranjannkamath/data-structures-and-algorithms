#include <algorithm>
#include <iostream>
#include <vector>

using std::vector;

void explore_to_visit(vector<vector<int> > &adj, vector<int> &is_visited, int x) {
    is_visited.at(x) = 1;
    for(int i = 0; i < adj.at(x).size(); ++i) {
        if(!is_visited.at(adj.at(x).at(i))) {
            explore_to_visit(adj, is_visited, adj.at(x).at(i));
        }
    }
}

void explore(vector<vector<int> > &adj, vector<int> &pre, vector<int> &post, int &order, int x) {
    if(pre.at(x) == 0) {
        pre.at(x) = ++order;
    }
    for(int i = 0; i < adj.at(x).size(); ++i) {
        if(pre.at(adj.at(x).at(i)) == 0) {
            explore(adj, pre, post, order, adj.at(x).at(i));
        }
    }
    if(post.at(x) == 0) {
        post.at(x) = ++order;
    }
}

int number_of_strongly_connected_components(vector<vector<int> > &adj, vector<vector<int> > &rev) {
    int result = 0;
    int order = 0;
    vector<int> pre(adj.size(), 0), post(adj.size(), 0), is_visited(adj.size(), 0);
    for(int i = 0; i < adj.size(); ++i) {
        explore(adj, pre, post, order, i);
    }
    int max_index, max = 0;
    // for(int i = 0; i < adj.size(); ++i) {
    //     std::cout << pre.at(i) << ' ';
    // }
    // std::cout << '\n';
    // for(int i = 0; i < adj.size(); ++i) {
    //     std::cout << post.at(i) << ' ';
    // }
    // std::cout << '\n';
    while(max != -1) {   
        max = -1;
        for(int j = 0; j < adj.size(); ++j) {
            if(!is_visited.at(j) && post.at(j) > max) {
                max = post.at(j);
                max_index = j;
            }
        }
        // std::cout << max_index << '\n';
        // for(int j = 0; j < adj.size(); ++j) {
        //     std::cout << is_visited.at(j) << ' ';
        // }
        // std::cout << '\n';
        explore_to_visit(rev, is_visited, max_index);
        if(max != -1) {
            result++;
        }
    }

    return result;
}

int main() {
    size_t n, m;
    std::cin >> n >> m;
    vector<vector<int> > adj(n, vector<int>());
    vector<vector<int> > rev(n, vector<int>());
    for (size_t i = 0; i < m; i++) {
        int x, y;
        std::cin >> x >> y;
        adj[x - 1].push_back(y - 1);
        rev[y - 1].push_back(x - 1);
    }
    std::cout << number_of_strongly_connected_components(adj, rev) << '\n';
}
