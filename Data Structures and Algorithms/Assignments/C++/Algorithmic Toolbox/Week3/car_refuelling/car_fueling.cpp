#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::vector;
using std::max;

int compute_min_refills(int dist, int tank, vector<int> stops) {
    int last_refill = 0;
    int current_refill = 0;
    int num_fills = 0;
    int dist_travelled = 0;

    stops.insert(stops.begin(), 0);
    stops.push_back(dist);

    while(dist_travelled <= dist) {

        while(stops.at(current_refill + 1) - stops.at(last_refill)  <= tank) {

            current_refill++;
            dist_travelled = stops.at(current_refill);

            if(current_refill == stops.size() - 1) {
                return num_fills;
            }
        }

        if(last_refill == current_refill) {
            return -1;
        }
        else {
            last_refill = current_refill;
            num_fills++;
        }
    }
}


int main() {
    int d = 0;
    cin >> d;
    int m = 0;
    cin >> m;
    int n = 0;
    cin >> n;

    vector<int> stops(n);
    for (size_t i = 0; i < n; ++i)
        cin >> stops.at(i);

    cout << compute_min_refills(d, m, stops) << "\n";
   

    return 0;
}
