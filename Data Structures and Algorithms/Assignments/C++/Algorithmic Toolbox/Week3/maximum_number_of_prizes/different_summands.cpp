#include <iostream>
#include <vector>

using std::vector;

vector<int> optimal_summands(int n) {

    vector<int> summands;
    int candies_remaining = n;

    if(n == 1) {
        summands.push_back(1);
        return summands;
    }

    for(int i = 1; i < n; ++i) {
        summands.push_back(i);
        candies_remaining -= i;
        if(summands.at(summands.size() - 1) >= candies_remaining) {
            summands.at(summands.size() - 1) += candies_remaining;
            return summands;
        }
    }
}

int main() {
    int n;
    std::cin >> n;
    vector<int> summands = optimal_summands(n);
    std::cout << summands.size() << '\n';
    for (size_t i = 0; i < summands.size(); ++i) {
        std::cout << summands[i] << ' ';
    }
    std::cout << std::endl;
}
