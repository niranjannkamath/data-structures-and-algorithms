#include <iostream>
#include <vector>

using std::vector;

double get_optimal_value(int capacity, vector<int> weights, vector<int> values) {
    double value = 0.0;
    int max_index = 0;

    vector<double> value_per_unit(weights.size());

    for(int i = 0; i < weights.size(); i++) {
            value_per_unit.at(i) = ((double) values.at(i)) / weights.at(i);
        }

    while(capacity >= 0) {

        for(int i = 0; i < weights.size(); i++) {
            if(value_per_unit.at(i) > value_per_unit.at(max_index)){
                max_index = i;
            }
        }
        if(weights.at(max_index) >= capacity) {
            value += capacity * value_per_unit.at(max_index);
            return value;
        }
        else {
            value += weights.at(max_index) * value_per_unit.at(max_index);
            capacity -= weights.at(max_index);
            value_per_unit.at(max_index) = 0.0;
        }
    }
}

int main() {
    int n;
    int capacity;
    std::cin >> n >> capacity;
    vector<int> values(n);
    vector<int> weights(n);
    for (int i = 0; i < n; i++) {
        std::cin >> values[i] >> weights[i];
    }

    double optimal_value = get_optimal_value(capacity, weights, values);

    std::cout.precision(10);
    std::cout << optimal_value << std::endl;
    return 0;
}
