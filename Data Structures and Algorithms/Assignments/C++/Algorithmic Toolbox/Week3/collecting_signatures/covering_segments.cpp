#include <algorithm>
#include <iostream>
#include <climits>
#include <vector>

using std::vector;
using std::sort;

struct Segment {
    int start, end;
};

void sort_segment(vector<Segment> &segments) {
    int min_index;
    Segment temp;
    for(int i = 0; i < segments.size() - 1; ++i) {
        min_index = i;
        for(int j = i + 1; j < segments.size(); ++j) {
            if(segments.at(j).end < segments.at(min_index).end) {
                min_index = j;
            }
        }
        temp = segments.at(i);
        segments.at(i) = segments.at(min_index);
        segments.at(min_index) = temp;
    }
}

vector<int> optimal_points(vector<Segment> &segments) {
    vector<int> points;

    int current_segment = 0, i = 0;

    while(i < segments.size()) {
        while(segments.at(current_segment).start <= segments.at(i).end && segments.at(current_segment).end >= segments.at(i).end) {
            current_segment++;
            if(current_segment == segments.size()) {
                points.push_back(segments.at(i).end);
                return points;
            }
        }
        points.push_back(segments.at(i).end);
        i = current_segment;
    }
}

int main() {
    int n;
    std::cin >> n;
    vector<Segment> segments(n);
    for (size_t i = 0; i < segments.size(); ++i) {
        std::cin >> segments[i].start >> segments[i].end;
    }

    sort_segment(segments);
    
    // for(int i = 0; i < segments.size(); ++i) {
    //     std::cout << segments.at(i).start << " " << segments.at(i).end << std::endl;
    // }
    vector<int> points = optimal_points(segments);
    std::cout << points.size() << "\n";
    for (size_t i = 0; i < points.size(); ++i) {
        std::cout << points[i] << " ";
    }
    std::cout << std::endl;
    
    return 0;
}