#include <algorithm>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::string;
using std::stoi;
using std::to_string;

bool check_greater_number(int num1, int num2) {
    string str1 = to_string(num1);
    string str2 = to_string(num2);
    
    string str_num1 = str1 + str2;
    string str_num2 = str2 + str1;

    int new_num1 = stoi(str_num1);
    int new_num2 = stoi(str_num2);

    if(new_num1 >= new_num2) {
        return true;
    }
    else {
        return false;
    }
}

string largest_number(vector<int> a) {

    int greater_index;
    int temp;
    
    for(int i = 0; i < a.size() - 1; ++i) {
        greater_index = i;
        for(int j = i + 1; j < a.size(); ++j) {
            if(check_greater_number(a.at(j), a.at(greater_index))) {
                greater_index = j;
            }
        }
        temp = a.at(i);
        a.at(i) = a.at(greater_index);
        a.at(greater_index) = temp;
    }

    std::stringstream ret;
    for(int j = 0; j < a.size(); ++j) {
        ret << a.at(j);
    }
    string result;
    ret >> result;

    return result;
}

int main() {
    int n;
    std::cin >> n;
    vector<int> a(n);
    for (size_t i = 0; i < a.size(); i++) {
        std::cin >> a[i];
    }
    std::cout << largest_number(a) << std::endl;

    return 0;
}