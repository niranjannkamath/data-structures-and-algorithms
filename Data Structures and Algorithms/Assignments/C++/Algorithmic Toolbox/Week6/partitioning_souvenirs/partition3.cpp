#include <iostream>
#include <vector>

using std::vector;

bool is_partition_possible(vector<int> &A, vector<int> &subset, int &sub_sum, int k) {
    int sum = 0, s = 0, n = A.size();

    for(int i = 0; i < n; ++i) {
        sum += A.at(i);
    }

    if(sum % k != 0 || n < k) {
        return false;
    }

    if(k == 1) {
        return true;
    }

    s = sum / k;

    bool dp_arr[n + 1][s + 1];

    for(int i = 0; i < n + 1; ++i) {
        dp_arr[i][0] = true;
    }

    for(int i = 1; i < s + 1; ++i) {
        dp_arr[0][i] = false;
    }

    for(int i = 1; i < n + 1; ++i) {
        for(int j = 1; j < s + 1; ++j) {
            dp_arr[i][j] = dp_arr[i - 1][j];
            if(A.at(i - 1) <= j) {
                dp_arr[i][j] = dp_arr[i][j - A.at(i - 1)] || dp_arr[i][j];
            }
        }  
    }

    for(int i = 0; i < n + 1; ++i) {
        for(int j = 0; j < s + 1; ++j) {
            std::cout << dp_arr[i][j] << ' ';
        }
        std::cout << '\n';
    }
    int p;
    for(p = n; p >= 0; --p) {
        if(dp_arr[p][s]) {
            break;
        }
    }

    if(p != -1) {
        int k = p;
        int temp = s;
        while(k > 0 && temp >= 0) {
            if(dp_arr[k][temp] == dp_arr[k - 1][temp]) {
                k--;
            }
            else {
                subset.push_back(k - 1);
                temp -= A.at(k - 1);
                k--;
            }
        }
    }
    
    // for(int i = 0; i < subset.size(); ++i) {
    //     std::cout << A.at(subset.at(i)) << ' ';
    // }

    return dp_arr[n][s];
}

int main() {
    int n;
    std::cin >> n;
    vector<int> A(n);
    vector<int> subset;
    int sub_sum = 0;
    for (size_t i = 0; i < A.size(); ++i) {
        std::cin >> A[i];
    }
    std::cout << is_partition_possible(A, subset, sub_sum, 3) << '\n';
}
