#include <iostream>
#include <vector>

using std::vector;

vector<bool> optimal_weight(int W, const vector<int> &w) {
    vector<vector<int> > weights(W + 1, vector<int> (w.size() + 1));
    vector<bool> items_not_taken(w.size(), false);

    for(int i = 0; i < W + 1; ++i) {
        weights[i][0] = 0;
    }
    for(int i = 0; i < w.size() + 1; ++i) {
        weights[0][i] = 0;
    }

    for(int i = 1; i < w.size() + 1; ++i) {
        for(int j = 1; j < W + 1; ++j) {
            weights[j][i] = weights[j][i - 1];
            if(w.at(i - 1) <= j && weights[j - w.at(i - 1)][i - 1] + w.at(i - 1) > weights[j][i]) {
                weights[j][i] = weights[j - w.at(i - 1)][i - 1] + w.at(i - 1);
            }
            //std::cout << weights[j][i] << ' ';
        }
        //std::cout << '\n';
    }

    for(int i = w.size(); i > 0; --i) {
        if(weights[W][i - 1] == weights[W][i]) {
            items_not_taken.at(i - 1) = true;
        }
        else {
            W -= w.at(i - 1);
        }
    }
    
    return items_not_taken;
}

int main() {
    int n, W;
    std::cin >> W >> n;
    vector<int> w(n);
    for (int i = 0; i < n; i++) {
        std::cin >> w[i];
    }
    vector<bool> items_not_taken = optimal_weight(W, w);
    for(int i = 0; i < w.size(); ++i) {
        if(!items_not_taken.at(i)) {
            std::cout << w.at(i) << ' ';
        }
    }
    std::cout << '\n';
}