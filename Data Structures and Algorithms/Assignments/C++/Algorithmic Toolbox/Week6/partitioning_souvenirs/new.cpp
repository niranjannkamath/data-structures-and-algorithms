#include <iostream>
#include <vector>

using std::vector;

struct optimal_solution {
    bool is_capacity;
    vector<bool> items_not_taken;
};

optimal_solution optimal_weight(int W, const vector<int> &w) {
    vector<vector<int> > weights(W + 1, vector<int> (w.size() + 1));
    vector<bool> items_not_taken(w.size(), false);

    for(int i = 0; i < W + 1; ++i) {
        weights[i][0] = 0;
    }
    for(int i = 0; i < w.size() + 1; ++i) {
        weights[0][i] = 0;
    }

    for(int i = 1; i < w.size() + 1; ++i) {
        for(int j = 1; j < W + 1; ++j) {
            weights[j][i] = weights[j][i - 1];
            if(w.at(i - 1) <= j && weights[j - w.at(i - 1)][i - 1] + w.at(i - 1) > weights[j][i]) {
                weights[j][i] = weights[j - w.at(i - 1)][i - 1] + w.at(i - 1);
            }
        }
    }
    int capacity = W, total = 0;
    for(int i = w.size(); i > 0; --i) {
        if(weights[capacity][i - 1] == weights[capacity][i]) {
            items_not_taken.at(i - 1) = true;
        }
        else {
            capacity -= w.at(i - 1);
        }
    }
    for(int i = 0; i < items_not_taken.size(); ++i) {
        if(!items_not_taken.at(i)) {
            total += w.at(i);
        }
    }

    optimal_solution opt;
    opt.is_capacity = (total == W);
    opt.items_not_taken = items_not_taken;

    return opt;
}

int partition3(vector<int> &A) {
    int sum = 0, s;
    for(int i = 0; i < A.size(); ++i) {
        sum += A.at(i);
    }
    if(sum % 3 != 0 || A.size() < 3) {
        return 0;
    }
    bool sums[A.size()][sum + 1];
    for(int i = 0; i < sum + 1; ++i) {
        sums[0][i] = false;
        sums[1][i] = false;
        sums[2][i] = false;
    }
    return 0;
}

int main() {
    int n;
    std::cin >> n;
    vector<int> A(n);
    for (size_t i = 0; i < A.size(); ++i) {
        std::cin >> A[i];
    }
    std::cout << partition3(A) << '\n';
}