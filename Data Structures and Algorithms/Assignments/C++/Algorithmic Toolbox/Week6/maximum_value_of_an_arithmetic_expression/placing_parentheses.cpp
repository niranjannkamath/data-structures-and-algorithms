#include <iostream>
#include <cassert>
#include <string>
#include <vector>
#include <climits>
#include <algorithm>

using std::vector;
using std::string;
using std::max;
using std::min;
using std::stoi;

long long eval(long long a, long long b, char op) {
    if (op == '*') {
        return a * b;
    } else if (op == '+') {
        return a + b;
    } else if (op == '-') {
        return a - b;
    } else {
        assert(0);
    }
}

long long get_maximum_value(string &exp) {
    
    vector<char> op;
    vector<int> nums;
    string s;
    long long minimum, maximum;
    int j; 
    long long a, b, c, d;

    if(exp.length() == 1) {
        s = string(1, exp.at(0));
        return stoi(s);
    }

    int n = (exp.length() - 1) / 2;
    long long mins[n + 1][n + 1];
    long long maxs[n + 1][n + 1];

    for(int i = 0; i < exp.size(); ++i) {
        if(i % 2 == 0) {
            s = string(1, exp.at(i));
            nums.push_back(stoi(s));
        }
        else {
            op.push_back(exp.at(i));
        }
    }

    for(int i = 0; i < n + 1; ++i) {
        maxs[i][i] = nums.at(i);
        mins[i][i] = nums.at(i);
    }
    for(int p = 1; p < n + 1; ++p){
        for(int i = 0; i < n + 1 - p; ++i) {
            j = i + p;
            minimum = LLONG_MAX;
            maximum = LLONG_MIN;
            for(int k = i; k < j; ++k) {
                a = eval(mins[i][k], mins[k + 1][j], op.at(k));
                b = eval(mins[i][k], maxs[k + 1][j], op.at(k));
                c = eval(maxs[i][k], mins[k + 1][j], op.at(k));
                d = eval(maxs[i][k], maxs[k + 1][j], op.at(k));
                minimum = min(std::initializer_list<long long>({minimum, a, b, c, d}));
                maximum = max(std::initializer_list<long long>({maximum, a, b, c, d}));
            }
            mins[i][j] = minimum;
            maxs[i][j] = maximum;
        } 
    }

    return maxs[0][n];
}

int main() {
    string s;
    std::cin >> s;
    std::cout << get_maximum_value(s) << '\n';
}
