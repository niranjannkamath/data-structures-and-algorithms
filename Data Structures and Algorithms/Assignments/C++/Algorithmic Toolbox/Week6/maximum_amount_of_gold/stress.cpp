#include <iostream>
#include <vector>
#include <ctime>
#include <cstdio>

using std::vector;
using std::srand;
using std::time;
using std::rand;

int optimal_weight(int W, const vector<int> &w) {
    vector<vector<int> > weights(W + 1, vector<int> (w.size() + 1));

    for(int i = 0; i < W + 1; ++i) {
        weights[i][0] = 0;
    }
    for(int i = 0; i < w.size() + 1; ++i) {
        weights[0][i] = 0;
    }

    for(int i = 1; i < w.size() + 1; ++i) {
        for(int j = 1; j < W + 1; ++j) {
            weights[j][i] = weights[j][i - 1];
            if(w.at(i - 1) <= j && weights[j - w.at(i - 1)][i - 1] + w.at(i - 1) > weights[j][i]) {
                weights[j][i] = weights[j - w.at(i - 1)][i - 1] + w.at(i - 1);
            }
        }
    }
    return weights[W][w.size()];
}

int main() {
    // int n, W;
    // std::cin >> W >> n;
    vector<int> w;
    srand((unsigned) time(0));
    
    while(true) {
        int n = rand() % 300 + 1;
        int W = rand() % 10000 + 1;
        for (int i = 0; i < n; i++) {
            w.push_back(rand() % 100001);
        }
        std::cout << W << ' ' << n << '\n';
        for(int i = 0; i < w.size(); ++i) {
            std::cout << w.at(i) << ' ';
        }
        std::cout << '\n';
        std::cout << optimal_weight(W, w) << '\n' << '\n' << '\n';
    }
}