#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cin;
using std::cout;

long long MaxPairwiseProduct(const vector<int>& numbers) {
    long long max_product = 0;
    int max_1;
    int max_2;
    int n = numbers.size();

    if(numbers[0] > numbers[1]) {
        max_1 = numbers[0];
        max_2 = numbers[1];
    }
    else {
        max_2 = numbers[0];
        max_1 = numbers[1];
    }

    for(int i = 0; i < n; i++) {
        if(numbers[i] > max_2) {
            max_1 = max_2;
            max_2 = numbers[i];
        }
        else if(numbers[i] > max_1) {
            max_1 = numbers[i];
        }
    }

    max_product = ((long long)max_1) * max_2;

    return max_product;
}

int main() {
    
    int n;
    cin >> n;

    vector<int> nums(n);

    for(int i = 0; i < n; i++) {
        cin >> nums[i];
    }

    long long result = MaxPairwiseProduct(nums);
    cout << result << "\n";

    return 0;
}
