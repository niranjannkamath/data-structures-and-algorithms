#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using std::vector;
using std::min;

vector<int> optimal_sequence(int n) {
    std::vector<int> sequence;
    std::vector<int> min_ops(n + 1, UINT16_MAX);
    min_ops.at(0) = 0;
    min_ops.at(1) = 0;

    int num;

    if(n == 1) {
        sequence.push_back(0);
        return sequence;
    }
    else {
        for(int i = 2; i < n + 1; ++i) {
            num = i; 
            if(num % 3 == 0 && num >= 3) {
                min_ops.at(i) = min(min_ops.at(i), min_ops.at(num/3) + 1);
            }
            if(num >= 2 && num % 2 == 0) {
                min_ops.at(i) = min(min_ops.at(i), min_ops.at(num/2) + 1);
            }
            if(num >= 2) {
                min_ops.at(i) = min(min_ops.at(i), min_ops.at(num - 1) + 1);
            }
        }  
    }
    num = n;
    while(num >= 1) {
        sequence.push_back(num);
        if(num >= 3 && num % 3 == 0 && min_ops.at(num) == min_ops.at(num/3) + 1) {
            num /= 3;
        }
        else if(num >= 2 && num % 2 == 0 && min_ops.at(num) == min_ops.at(num/2) + 1) {
            num /= 2;
        }
        else{
            num -= 1;
        }
    }
    reverse(sequence.begin(), sequence.end());
    return sequence;
}

int main() {
    int n;
    std::cin >> n;
    vector<int> sequence = optimal_sequence(n);
    std::cout << sequence.size() - 1 << std::endl;
    for (size_t i = 0; i < sequence.size(); ++i) {
        std::cout << sequence[i] << " ";
    }

    std::cout << std::endl;

    return 0;
}
