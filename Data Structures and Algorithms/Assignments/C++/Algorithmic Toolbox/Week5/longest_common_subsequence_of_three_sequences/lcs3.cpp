#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::max;

vector<int> lcs2(vector<int> &a, vector<int> &b) {
    
    if(a.empty() || b.empty()) {
        return a;
    }

    int lcs[a.size()][b.size()];

    bool is_equal = false;

    for(int i = 0; i < a.size(); ++i) {
        if(a.at(i) == b.at(0)) {
            is_equal = true;
        }
        lcs[i][0] = is_equal;
    }
    is_equal = false;
    for(int i = 0; i < b.size(); ++i) {
        if(b.at(i) == a.at(0)) {
            is_equal = true;
        }
        lcs[0][i] = is_equal;
    }

    for(int i = 1; i < a.size(); ++i) {
        for(int j = 1; j < b.size(); ++j) {
            lcs[i][j] = 0;
            if(a.at(i) == b.at(j)) {
                lcs[i][j] = max(lcs[i][j], lcs[i - 1][j - 1] + 1);
                if(lcs[i][j - 1] == lcs[i - 1][j - 1]) {
                    lcs[i][j] = max(lcs[i][j], lcs[i][j - 1] + 1);
                }
                else {
                    lcs[i][j] = max(lcs[i][j], lcs[i][j - 1]);
                }
                if(lcs[i - 1][j] == lcs[i - 1][j - 1])  {
                    lcs[i][j] = max(lcs[i][j], lcs[i - 1][j] + 1);
                }
                else {
                    lcs[i][j] = max(lcs[i][j], lcs[i - 1][j]);
                }
            }
            else {
                lcs[i][j] = max(lcs[i - 1][j - 1], max(lcs[i - 1][j], lcs[i][j - 1]));
            }
        }
    }

    vector<int> subsequence;

    int i = 0;
    int j = 0;
    if(lcs[a.size() - 1][0] == 1) {
        subsequence.push_back(b.at(0));
    }

    int seq_len = lcs[a.size() - 1][0];
    for(int j = 1; j < b.size(); ++j) {
        if(lcs[a.size() - 1][j] != seq_len) {
            subsequence.push_back(b.at(j));
            seq_len = lcs[a.size() - 1][j];
        }
    }

    // for(int k = 0; k < subsequence.size(); ++k) {
    //     std::cout << subsequence.at(k) << ' ';
    // }

    return subsequence;
}

int lcs3(vector<int> &a, vector<int> &b, vector<int> &c) {
    vector<int> comp;
    int max_length, max1, max2, max3;

    comp = lcs2(a, b);
    if(comp.empty()) {
        return 0;
    }
    max1 = lcs2(comp, c).size();
    comp.clear();

    comp = lcs2(b, c);
    if(comp.empty()) {
        return 0;
    }
    max2 = lcs2(comp, a).size();
    comp.clear();

    comp = lcs2(c, a);
    if(comp.empty()) {
        return 0;
    }
    max3 = lcs2(comp, b).size();
    comp.clear();

    max_length = max(max1, max(max2, max3));

    return max_length;
}

int main() {
    size_t an;
    std::cin >> an;
    vector<int> a(an);
    for (size_t i = 0; i < an; i++) {
        std::cin >> a[i];
    }
    size_t bn;
    std::cin >> bn;
    vector<int> b(bn);
    for (size_t i = 0; i < bn; i++) {
        std::cin >> b[i];
    }
    size_t cn;
    std::cin >> cn;
    vector<int> c(cn);
    for (size_t i = 0; i < cn; i++) {
        std::cin >> c[i];
    }
    std::cout << lcs3(a, b, c) << std::endl;

    // lcs2(a, b);

    // std::cout << '\n';

    return 0;
}