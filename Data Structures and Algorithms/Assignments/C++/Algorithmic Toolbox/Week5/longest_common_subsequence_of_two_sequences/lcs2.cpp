#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::max;

int lcs2(vector<int> &a, vector<int> &b) {
    
    int lcs[a.size()][b.size()];

    bool is_equal = false;

    for(int i = 0; i < a.size(); ++i) {
        if(a.at(i) == b.at(0)) {
            is_equal = true;
        }
        lcs[i][0] = is_equal;
    }
    is_equal = false;
    for(int i = 0; i < b.size(); ++i) {
        if(b.at(i) == a.at(0)) {
            is_equal = true;
        }
        lcs[0][i] = is_equal;
    }

    for(int i = 1; i < a.size(); ++i) {
        for(int j = 1; j < b.size(); ++j) {
            lcs[i][j] = 0;
            if(a.at(i) == b.at(j)) {
                lcs[i][j] = max(lcs[i][j], lcs[i - 1][j - 1] + 1);
                if(lcs[i][j - 1] == lcs[i - 1][j - 1]) {
                    lcs[i][j] = max(lcs[i][j], lcs[i][j - 1] + 1);
                }
                else {
                    lcs[i][j] = max(lcs[i][j], lcs[i][j - 1]);
                }
                if(lcs[i - 1][j] == lcs[i - 1][j - 1])  {
                    lcs[i][j] = max(lcs[i][j], lcs[i - 1][j] + 1);
                }
                else {
                    lcs[i][j] = max(lcs[i][j], lcs[i - 1][j]);
                }
            }
            else {
                lcs[i][j] = max(lcs[i - 1][j - 1], max(lcs[i - 1][j], lcs[i][j - 1]));
            }
        }
    }

    for(int i = 0; i < a.size(); ++i) {
        for(int j = 0; j < b.size(); ++j) {
            std::cout << lcs[i][j] << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    return lcs[a.size() - 1][b.size() - 1];
}

int main() {
    size_t n;
    std::cin >> n;
    vector<int> a(n);
    for (size_t i = 0; i < n; i++) {
        std::cin >> a[i];
    }

    size_t m;
    std::cin >> m;
    vector<int> b(m);
    for (size_t i = 0; i < m; i++) {
        std::cin >> b[i];
    }

    std::cout << lcs2(a, b) << std::endl;
}
