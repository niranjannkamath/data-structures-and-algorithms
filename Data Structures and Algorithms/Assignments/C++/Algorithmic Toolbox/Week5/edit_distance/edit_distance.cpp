#include <iostream>
#include <string>
#include <algorithm>
#include <climits>

using std::string;
using std::min;

int edit_distance(const string &str1, const string &str2) {
    
    int n1 = str1.length();
    int n2 = str2.length();

    int ed[n1 + 1][n2 + 1];

    for(int i = 0; i < n1 + 1; ++i) {
        ed[i][0] = i;
    }
    for(int i = 0; i < n2 + 1; ++i) {
        ed[0][i] = i;
    }

    for(int i = 1; i < n1 + 1; ++i) {
        for(int j = 1; j < n2 + 1; ++j) {
            ed[i][j] = UINT16_MAX;
            ed[i][j] = min(ed[i][j], ed[i - 1][j] + 1);
            ed[i][j] = min(ed[i][j], ed[i][j - 1] + 1);
            if(str1.at(i - 1) == str2.at(j - 1)) {
                ed[i][j] = min(ed[i][j], ed[i - 1][j - 1]);
            }
            else {
                ed[i][j] = min(ed[i][j], ed[i - 1][j - 1] + 1);
            }
        }
    }

    return ed[n1][n2];
}

int main() {
    string str1;
    string str2;
    std::cin >> str1 >> str2;
    std::cout << edit_distance(str1, str2) << std::endl;
    return 0;
}
