#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using std::vector;
using std::min;

int get_change(int m) {

    vector<int> num_coins(m + 1, UINT16_MAX);
    num_coins.at(0) = 0;
    
    for(int i = 1; i < m + 1; ++i) {
        if(i - 1 >= 0) {
            num_coins.at(i) = min(num_coins.at(i), num_coins.at(i - 1) + 1);
        }
        if(i - 3 >= 0) {
            num_coins.at(i) = min(num_coins.at(i), num_coins.at(i - 3) + 1);
        }
        if(i - 4 >= 0) {
            num_coins.at(i) = min(num_coins.at(i), num_coins.at(i - 4) + 1);
        }
    }

    return num_coins.at(m);
}

int main() {
    int m;
    std::cin >> m;
    std::cout << get_change(m) << '\n';

    return 0;
}