#include <iostream>
#include <vector>
#include <cstdlib>

using std::vector;
using std::swap;

struct Partition {
    int m1; int m2;
};

Partition partition3(vector<int> &a, int l, int r) {
    int x = a[l];
    int j = l;
    int k;
    for (int i = l + 1; i <= r; i++) {
        if (a[i] < x) {
            j++;
            swap(a[i], a[j]);
        }
    }
    swap(a[l], a[j]);
    k = j;
    for(int i = j + 1; i <= r; i++) {
        if(a[i] == x) {
            k++;
            swap(a[i], a[k]);
        }
    }

    Partition partition;
    partition.m1 = j;
    partition.m2 = k;

    return partition;
}

void randomized_quick_sort(vector<int> &a, int l, int r) {
    if (l >= r) {
        return;
    }

    int k = l + rand() % (r - l + 1);
    swap(a[l], a[k]);
    Partition partition = partition3(a, l, r);

    int m1 = partition.m1;
    int m2 = partition.m2;

    randomized_quick_sort(a, l, m1 - 1);
    randomized_quick_sort(a, m2 + 1, r);
}

int main() {
    int n;
    std::cin >> n;
    vector<int> a(n);
    for (size_t i = 0; i < a.size(); ++i) {
        std::cin >> a[i];
    }
    randomized_quick_sort(a, 0, a.size() - 1);
    for (size_t i = 0; i < a.size(); ++i) {
        std::cout << a[i] << ' ';
    }
    std::cout << '\n';
}
