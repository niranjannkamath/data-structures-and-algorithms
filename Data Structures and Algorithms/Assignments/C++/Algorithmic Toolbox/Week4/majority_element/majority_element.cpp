#include <algorithm>
#include <iostream>
#include <vector>

using std::vector;

int get_count_by_key(vector<int> &a, int left, int right, int key) {
    int count = 0, mid = 0;
    if(right == left + 1) {
        if(a.at(left) == key) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        mid = (right - left) / 2 + left;
        count += get_count_by_key(a, left, mid, key); 
        count += get_count_by_key(a, mid, right, key);
        return count;
    }
}

int get_majority_element(vector<int> &a, int left, int right) {
    int count = 0;

    if (left == right) {
        return -1;
    }
    if (left + 1 == right) {
        return a[left];
    }

    for(int i = left; i < right; ++i) {
        count = get_count_by_key(a, left, right, a.at(i));
        if(count > a.size() / 2) {
            return 1;
        }
    }
    
    return -1;
}

int main() {
    int n = 100000;
    //std::cin >> n;
    vector<int> a(n);
    for (size_t i = 0; i < a.size(); ++i) {
        if(i % 2 == 0) {
            a[i] = 1;
        }
        else {
            a[i] = 2;
        }
    }
    std::cout << (get_majority_element(a, 0, a.size()) != -1) << '\n';
  
}
