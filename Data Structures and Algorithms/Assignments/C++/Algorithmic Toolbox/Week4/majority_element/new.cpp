#include <algorithm>
#include <iostream>
#include <vector>

using std::vector;

int get_majority_element(vector<int> &a, int left, int right) {
    int count = 0;
    if(right  == left + 1) {
        return a.at(left);
    }

    int mid = (right - left) / 2 + left;

    int p = get_majority_element(a, left, mid);
    int q = get_majority_element(a, mid, right);

    if(p != -1) {
        for(int i = left; i < right; ++i) {
            if(a.at(i) == p) {
                count++;
            }
        }
        if(count > (right - left) / 2) {
            return p;
        }
    }

    if(q != -1) {
        count = 0;
        for(int i = left; i < right; ++i) {
            if(a.at(i) == q) {
                count++;
            }
        }
        if(count > (right - left) / 2) {
            return q;
        }
    }
    
    return -1;
}

int main() {
    int n;
    std::cin >> n;
    vector<int> a(n);
    for (size_t i = 0; i < a.size(); ++i) {
        std::cin >> a[i];
    }
    std::cout << (get_majority_element(a, 0, a.size()) != -1) << '\n';
  
}