#include <algorithm>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include <cmath>

using std::vector;
using std::string;
using std::pair;
using std::min;
using std::min_element;


void sort_by_x(vector<pair<int, int> > &points, int left, int right) {

    vector<pair<int, int> > sorted(points.size());
    pair<int, int> temp;

    int mid = (right - left) / 2 + left;

    if(right == left + 1) {
        return;
    }
    else if(right == left + 2) {
        if(points.at(left).first <= points.at(left + 1).first) {
            return;
        }
        else {
            temp = points.at(left);
            points.at(left) = points.at(left + 1);
            points.at(left + 1) = temp;
            return;
        }
    }
    else {
        sort_by_x(points, left, mid);
        sort_by_x(points, mid, right);
    }
    int i = left;
    int j = mid;

    for(int k = left; k < right; ++k) {
        if(i < mid && j < right){
            if(points.at(i).first <= points.at(j).first) {
                sorted.at(k) = points.at(i);
                i++;
            }
            else {
                sorted.at(k) = points.at(j);
                j++;
            }
        }
        else if(i == mid && j != right) {
            sorted.at(k) = points.at(j);
            j++;
        }
        else if(i != mid && j == right) {
            sorted.at(k) = points.at(i);
            i++;
        }
    }
    for(int k = left; k < right; ++k) {
        points.at(k) = sorted.at(k);
    }
}

long double distance(pair<int, int> pt1, pair<int, int> pt2) {
    return pow(pow(pt2.first - pt1.first, 2) + pow(pt2.second - pt1.second, 2), 0.5);
}

long double find_min(vector<pair<int, int> > &points, int left, int right) {

    long double minimum = 0;

    vector<pair<int, int> > points_in_strip;

    int mid = (right - left) / 2 + left;

    if(right == left + 3) {
        long double d1 = distance(points.at(left), points.at(left + 1));
        long double d2 = distance(points.at(left), points.at(left + 2));
        long double d3 = distance(points.at(left + 2), points.at(left + 1));
        return min(d1, min(d2, d3));
    }
    else if(right == left + 2) {
        return distance(points.at(left), points.at(left + 1));
    }
    else {
        long double min1 = find_min(points, left, mid);
        long double min2 = find_min(points, mid, right);

        minimum = min(min1, min2);
    }
    
    int x_mid = points.at(mid).first;

    for(int i = left; i < right; ++i) {
        if(points.at(i).first < x_mid + minimum && points.at(i).first > x_mid - minimum) {
            points_in_strip.push_back(points.at(i));
        }
    }

    vector<long double> dist_in_strip;

    
    for(int i = 0; i < points_in_strip.size(); ++i) {
        for(int j = i + 1; j <= i + 7; ++j) {
            if(j < points_in_strip.size()) {
                dist_in_strip.push_back(distance(points_in_strip.at(i), points_in_strip.at(j)));
            }
            else {
                break;
            }
        }
    }
    long double strip_minimum = *min_element(dist_in_strip.begin(), dist_in_strip.end());
    minimum = min(minimum, strip_minimum);

    return minimum;
}

long double minimal_distance(vector<int> &x, vector<int> &y) {
    
    long double minimum = 0;

    vector<pair<int, int> > points(x.size());

    for(int i = 0; i < x.size(); ++i) {
        points.at(i).first = x.at(i);
        points.at(i).second = y.at(i);
    }

    sort_by_x(points, 0, points.size());

    minimum = find_min(points, 0, points.size());

    return minimum;
}

int main() {
    size_t n;
    std::cin >> n;
    vector<int> x(n);
    vector<int> y(n);
    for (size_t i = 0; i < n; i++) {
        std::cin >> x[i] >> y[i];
    }

    std::cout << std::fixed;
    std::cout << std::setprecision(9) << minimal_distance(x, y) << "\n";
    
    return 0;
}
