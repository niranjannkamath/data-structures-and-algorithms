#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::sort;

struct Segment {
    int start, end;
};

int search_higher(vector<int> &higher, int left, int right, int key) {

    int mid = (right - left) / 2 + left;

    int index = -1;

    if(right == left + 1) {
        if(higher.at(left) < key) {
            return left;
        }
        else {
            return -1;
        }
    }
    else if(key <= higher.at(mid)) {
        index = search_higher(higher, left, mid, key);
    }
    else if(key > higher.at(mid)) {
        index = search_higher(higher, mid, right, key);
    }

    return index;
}

int search_lower(vector<int> &lower, int left, int right, int key) {

    int mid = (right - left) / 2 + left;

    int index = -1;

    if(right == left + 1) {
        if(lower.at(left) > key) {
            return -1;
        }
        else {
            return left;
        }
    }
    else if(key < lower.at(mid)) {
        index = search_lower(lower, left, mid, key);
    }
    else if(key >= lower.at(mid)) {
        index = search_lower(lower, mid, right, key);
    }

    return index;
}

vector<int> fast_count_segments(vector<Segment> &segments, vector<int> &points) {

    vector<int> cnt(points.size());

    int lower_index;
    int higher_index;

    vector<int> lower;
    vector<int> higher;

    for(int i = 0; i < segments.size(); ++i) {
        lower.push_back(segments.at(i).start);
        higher.push_back(segments.at(i).end);
    }

    sort(lower.begin(), lower.end());
    sort(higher.begin(), higher.end());

    for(int i = 0; i < points.size(); ++i) {
        lower_index = search_lower(lower, 0, lower.size(), points.at(i));
        higher_index = search_higher(higher, 0, higher.size(), points.at(i));
        cnt.at(i) = lower_index - higher_index ;
    }

    return cnt;
}

int main() {
    int n, m;
    std::cin >> n >> m;
    vector<Segment> segments(n);
    for (size_t i = 0; i < segments.size(); i++) {
        std::cin >> segments[i].start >> segments[i].end;
    }
    vector<int> points(m);
    for (size_t i = 0; i < points.size(); i++) {
        std::cin >> points[i];
    }
    
    vector<int> cnt = fast_count_segments(segments, points);
    for (size_t i = 0; i < cnt.size(); i++) {
        std::cout << cnt[i] << ' ';
    }
    std::cout << '\n';

    return 0;
}