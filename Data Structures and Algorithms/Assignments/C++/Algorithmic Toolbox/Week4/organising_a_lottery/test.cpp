#include <iostream>
#include <vector>

using std::vector;

struct Segment {
    int start, end;
};

void sort_by_lower(vector<Segment> &segments, int left, int right) {

    Segment temp;
    vector<Segment> sorted(segments.size());

    int mid = (right - left) / 2 + left;

    if(right == left + 1) {
        return;
    }
    else if(right == left + 2) {
        if(segments.at(left).start <= segments.at(left + 1).start) {
            return;
        }
        else {
            temp = segments.at(left);
            segments.at(left) = segments.at(left + 1);
            segments.at(left + 1) = temp;
        }
    }
    else {
        sort_by_lower(segments, left, mid);
        sort_by_lower(segments, mid, right);
    }

    int i = left;
    int j = mid;

    for(int k = left; k < right; ++k) {
        if(i < mid && j < right) {
            if(segments.at(i).start <= segments.at(j).start) {
                sorted.at(k) = segments.at(i);
                i++;
            }
            else {
                sorted.at(k) = segments.at(j);
                j++;
            }
        }
        else if(i == mid && j != right) {
            sorted.at(k) = segments.at(j);
            j++;
        }
        else if(i != mid && j == right) {
            sorted.at(k) = segments.at(i);
            i++;
        }
    }
    for(int k = left; k < right; ++k) {
        segments.at(k) = sorted.at(k);
    }
}

int search(vector<Segment> &segments, int left, int right, int key) {

    int mid = (right - left) / 2 + left;

    int index = -1;

    if(right == left + 1) {
        return left;
    }
    else if(key < segments.at(mid).start) {
        index = search(segments, left, mid, key);
    }
    else if(key > segments.at(mid).start) {
        index = search(segments, mid, right, key);
    }
    else {
        return mid;
    }

    return index;
}

int main() {

    int n;
    std::cin >> n;

    vector<Segment> segments(n);

    for(int i = 0; i < n; ++i) {
        std::cin >> segments.at(i).start >> segments.at(i).end;
    }

    sort_by_lower(segments, 0, segments.size());

    std::cout << '\n';
    for(int i = 0; i < segments.size(); ++i) {
        std::cout << segments.at(i).start << ' ' << segments.at(i).end << '\n';
    }

    std::cout << '\n';
    std::cout << search(segments, 0, segments.size(), 100) << '\n';

    return 0;
}