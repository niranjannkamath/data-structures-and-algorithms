#include <iostream>
#include <vector>

using std::vector;
using std::swap;

long long get_number_of_inversions(vector<int> &a, vector<int> &b, size_t left, size_t right) {
    long long number_of_inversions = 0;
    size_t ave = left + (right - left) / 2;
    if (right <= left + 1) {
        return number_of_inversions;
    }
    else if(right == left + 2) {
        if(a.at(left) > a.at(left + 1)) {
            number_of_inversions++;
            swap(a.at(left), a.at(left + 1));
            return number_of_inversions;
        }
        else {
            return number_of_inversions;
        }
    }
    else {
        number_of_inversions += get_number_of_inversions(a, b, left, ave);
        number_of_inversions += get_number_of_inversions(a, b, ave, right);
        
    }
    int i = left, j = ave, p;
    bool is_counted = true;

    for(int k = left; k < right; ++k) {
        if(i < ave && j < right) {
            if(a.at(i) <= a.at(j)) {
                b.at(k) = a.at(i);
                if(is_counted) {
                    number_of_inversions += j - ave;
                }
                else {
                    is_counted = !is_counted;
                }
                i++;
            }
            else {
                b.at(k) = a.at(j);
                if(is_counted) {
                    number_of_inversions += j - ave + 1;
                    is_counted = !is_counted;
                }
                else {
                    number_of_inversions++;
                }
                j++;
            }
            p = i;
        }
        else if(i == ave && j != right) {
            b.at(k) = a.at(j);
            j++;
        }
        else if(i != ave && j == right) {
            b.at(k) = a.at(p);
            if(p >= i + 1) {
                number_of_inversions += right - ave;
            }
            p++;
        }
    }
    for(int k = left; k < right; ++k) {
        a.at(k) = b.at(k);
    }

    return number_of_inversions;
}

int main() {
    int n;
    std::cin >> n;
    vector<int> a(n);
    for (size_t i = 0; i < a.size(); i++) {
        std::cin >> a[i];
    }
    vector<int> b(a.size());

    std::cout << get_number_of_inversions(a, b, 0, a.size()) << '\n';

    // for(int i = 0; i < a.size(); ++i) { 
    //     std::cout << a.at(i) << ' ';
    // }
    // std::cout << '\n';

    return 0;
}