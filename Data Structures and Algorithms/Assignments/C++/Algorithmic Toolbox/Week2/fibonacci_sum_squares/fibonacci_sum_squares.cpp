#include <iostream>

int get_fibonacci_last_digit(long long n) {

    int f0_last {0};
    int f1_last {1};
    int fib_last_digit {0};

    if(n <= 1) {
        return n;
    }

    else {

        for (long long i = 0; i < n - 1; ++i) {
            fib_last_digit = (f0_last + f1_last) % 10;
            f0_last = f1_last;
            f1_last = fib_last_digit;
        }

        return fib_last_digit;

    }
}

int fibonacci_sum_squares(long long n) {
    int f1 = get_fibonacci_last_digit(n % 60);
    int f2 = get_fibonacci_last_digit((n + 1) % 60);

    return (f1 * f2) % 10;
}

int main() {
    long long n = 0;
    std::cin >> n;
    std::cout << fibonacci_sum_squares(n) << '\n';
}
