#include <iostream>

int gcd(int a, int b) {
    
    int rem  {0};

    if (b == 0) {
      return a;
    }
    else if (b < a){
      rem = a % b;
      return gcd(b, rem);
    }
    else {
      rem = b % a;
      return gcd(a, rem);
    }
}

long long lcm(long long a, long long b) {

    long long lcm {0};
    int gcd_num = gcd((int)a, (int)b);

    lcm = (a * b) / gcd_num;

    return lcm;
}

int main() {

    int a, b;

    std::cin >> a >> b;
    std::cout << lcm(a, b) << std::endl;

    return 0;
}
