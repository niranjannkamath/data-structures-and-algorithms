#include <iostream>

int get_fibonacci_last_digit(int n) {

    int f0_last {0};
    int f1_last {1};
    int fib_last_digit {0};

    if(n <= 1) {
        return n;
    }

    else {

        for (int i = 0; i < n - 1; ++i) {
            fib_last_digit = (f0_last + f1_last) % 10;
            f0_last = f1_last;
            f1_last = fib_last_digit;
        }

        return fib_last_digit;

    }
}

int main() {
    int n;
    std::cin >> n;
    int c = get_fibonacci_last_digit(n);
    std::cout << c << '\n';
}
