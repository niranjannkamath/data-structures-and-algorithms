#include <iostream>

int gcd(int a, int b) {
    
    int rem = 0;

    if (b == 0) {
      return a;
    }
    else if (b < a){
      rem = a % b;
      return gcd(b, rem);
    }
    else {
      rem = b % a;
      return gcd(a, rem);
    }
}

int main() {

    int a, b;
    std::cin >> a >> b;
    std::cout << gcd(a, b) << std::endl;

    return 0;
}