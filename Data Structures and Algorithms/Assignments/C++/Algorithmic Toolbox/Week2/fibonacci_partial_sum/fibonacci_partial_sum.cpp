#include <iostream>
#include <vector>

using std::vector;

int get_fibonacci_last_digit(int n) {

    int f0_last {0};
    int f1_last {1};
    int fib_last_digit {0};

    if(n <= 1) {
        return n;
    }

    else {

        for (int i = 0; i < n - 1; ++i) {
            fib_last_digit = (f0_last + f1_last) % 10;
            f0_last = f1_last;
            f1_last = fib_last_digit;
        }

        return fib_last_digit;

    }
}

int get_fibonacci_partial_sum(long long m, long long n) {

    int f1 = get_fibonacci_last_digit((n - m + 1) % 60);
    int f2 = get_fibonacci_last_digit(m % 60);
    int f3 = get_fibonacci_last_digit((n - m + 2) % 60);
    int f4 = get_fibonacci_last_digit((m + 1) % 60);

    int last_digit = (f1 * f2 + f3 * f4 - f4) % 10;

    return last_digit;
}

int main() {
    long long from, to;
    std::cin >> from >> to;
    std::cout << get_fibonacci_partial_sum(from, to) << '\n';
}
