#include <iostream>
#include <vector>

using std::vector;

long long get_fibonacci(int n) {
    long long f0 {0};
    long long f1 {1};
    long long fib {0};

    if(n <= 1) {
        return n;
    }

    for(int i = 0; i < n - 1; i++) {
        fib = f0 + f1;
        f0 = f1;
        f1 = fib;
    }

    return fib;
}

int fibonacci_sum(long long n) {

    int remainder = (get_fibonacci((n + 2) % 60) - 1) % 10;

    return remainder;
}

int main() {
    long long n = 0;
    std::cin >> n;
    std::cout << fibonacci_sum(n) << '\n';
}
