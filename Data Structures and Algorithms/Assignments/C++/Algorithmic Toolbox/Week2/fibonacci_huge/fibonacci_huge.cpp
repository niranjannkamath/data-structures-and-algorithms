#include <iostream>
#include <vector>

using std::vector;

int get_fibonacci_huge(long long n, long long m) {

    int period {0};
    vector <int> fib;
    fib.push_back(0);
    fib.push_back(1);

    if(n <= 1) {
        return n;
    }
    else {
        long long i = 0;
        while(true) {
            fib.push_back((fib.at(fib.size() - 2) % m + fib.at(fib.size() - 1) % m) % m);

            if(fib.at(fib.size() - 1) == 0) {
                if((fib.at(fib.size() - 2) + fib.at(fib.size() - 1)) % m == 1) {
                    period = i + 2;
                    break;
                }
            }

            i++;
        }
    }

    return fib.at(n % period);
}

int main() {
    long long n, m;
    std::cin >> n >> m;
    std::cout << get_fibonacci_huge(n, m) << '\n';
}
