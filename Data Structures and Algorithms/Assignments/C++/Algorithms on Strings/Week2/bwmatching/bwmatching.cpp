#include <algorithm>
#include <cstdio>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

using std::cin;
using std::istringstream;
using std::map;
using std::string;
using std::pair;
using std::vector;

const int LETTERS = 4;
typedef map<char, vector<int> >::iterator map_it;
typedef map<char, vector<int> >::const_iterator map_const_it;

int char_to_int(char x) {
    switch(x) {
        case 'A' :
            return 0;
        case 'C' :
            return 1;
        case 'G' :
            return 2;
        case 'T' :
            return 3;
    }
}

char int_to_char(int x) {
    switch(x) {
        case 0 :
            return 'A';
        case 1 :
            return 'C';
        case 2 :
            return 'G';
        case 3 :
            return 'T';
    }
}

// Preprocess the Burrows-Wheeler Transform bwt of some text
// and compute as a result:
//   * starts - for each character C in bwt, starts[C] is the first position 
//       of this character in the sorted array of 
//       all characters of the text.
//   * occ_count_before - for each character C in bwt and each position P in bwt,
//       occ_count_before[C][P] is the number of occurrences of character C in bwt
//       from position 0 to position P inclusive.
void PreprocessBWT(const string& bwt, 
                   map<char, int>& starts, 
                   map<char, vector<int> >& occ_count_before) {
    int start = 1, counts[LETTERS];
    pair<char, int> p_start;
    pair<char, vector<int> > p_occ;
    map_it it;
    for(int i = 0; i < LETTERS; ++i) {
        counts[i] = 0;
    }
    for(int i = 0; i < bwt.size(); ++i) {
        if(bwt.at(i) != '$') {
            counts[char_to_int(bwt.at(i))]++;
            it = occ_count_before.find(bwt.at(i));
            if(it == occ_count_before.end()) {
                p_occ.first = bwt.at(i);
                p_occ.second = vector<int>(bwt.size() + 1, 0);
                p_occ.second.at(i + 1) = counts[char_to_int(bwt.at(i))];
                occ_count_before.insert(p_occ);
            }            
        }
        for(int j = 0; j < LETTERS; ++j) {
            it = occ_count_before.find(int_to_char(j));
            if(it != occ_count_before.end()) {
                it->second.at(i + 1) = counts[j];
            }
        }
    }
    for(int i = 0; i < LETTERS; ++i) {
        if(counts[i]) {
            p_start.first = int_to_char(i);
            p_start.second = start;
            starts.insert(p_start);
            start += counts[i];
        }
    }
}

// Compute the number of occurrences of string pattern in the text
// given only Burrows-Wheeler Transform bwt of the text and additional
// information we get from the preprocessing stage - starts and occ_counts_before.
int CountOccurrences(const string& pattern, 
                     const string& bwt, 
                     const map<char, int>& starts, 
                     const map<char, vector<int> >& occ_count_before) {
    int top = 0;
    int bottom = bwt.size() - 1;
    int current_pos = pattern.size() - 1;
    char current_symbol;
    map_const_it it;
    while(top <= bottom) {
        if(current_pos >= 0) {
            current_symbol = pattern.at(current_pos);
            it = occ_count_before.find(current_symbol);
            if(it != occ_count_before.end() && it->second.at(bottom + 1) - it->second.at(top) > 0) {
                top = starts.find(current_symbol)->second + it->second.at(top);
                bottom = starts.find(current_symbol)->second + it->second.at(bottom + 1) - 1;
            }
            else {
                return 0;
            }
            current_pos--;
        }
        else {
            return bottom - top + 1;
        }
    }
}
     

int main() {
    string bwt;
    cin >> bwt;
    int pattern_count;
    cin >> pattern_count;
    // Start of each character in the sorted list of characters of bwt,
    // see the description in the comment about function PreprocessBWT
    map<char, int> starts;
    // Occurrence counts for each character and each position in bwt,
    // see the description in the comment about function PreprocessBWT
    map<char, vector<int> > occ_count_before;
    // Preprocess the BWT once to get starts and occ_count_before.
    // For each pattern, we will then use these precomputed values and
    // spend only O(|pattern|) to find all occurrences of the pattern
    // in the text instead of O(|pattern| + |text|).
    PreprocessBWT(bwt, starts, occ_count_before);
    for (int pi = 0; pi < pattern_count; ++pi) {
        string pattern;
        cin >> pattern;
        int occ_count = CountOccurrences(pattern, bwt, starts, occ_count_before);
        printf("%d ", occ_count);
    }
    printf("\n");
    return 0;
}
