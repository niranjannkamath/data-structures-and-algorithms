#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::sort;

string BWT(const string& text) {
    string result = "";
    vector<string> bwt_matrix(text.size(), "");

    for(int i = 0; i < text.size(); ++i) {
        for(int j = 0; j < text.size(); ++j) {
            bwt_matrix.at(j) += text.at((j + i) % text.size());
        }
    }
    sort(bwt_matrix.begin(), bwt_matrix.end());
    for(int i = 0; i < bwt_matrix.size(); ++i) {
        result += bwt_matrix.at(i).at(text.size() - 1);
    }
    return result;
}

int main() {
    string text;
    cin >> text;
    cout << BWT(text) << endl;
    return 0;
}