#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

const int LETTERS = 4;

int char_to_int(char x) {
    switch(x) {
        case 'A':
            return 0;
        case 'C':
            return 1;
        case 'G' :
            return 2;
        case 'T' :
            return 3;
    }
}

char int_to_char(int x) {
    switch(x) {
        case 0:
            return 'A';
        case 1:
            return 'C';
        case 2 :
            return 'G';
        case 3 :
            return 'T';
    }
}

string InverseBWT(const string& bwt) {
    string text = "";
    int counter, start = 1, counts[LETTERS], start_pos[LETTERS];
    vector<char> initials;
    vector<vector<int> > count_matrix(bwt.size(), vector<int>(LETTERS, 0));
    char current, prev;
    for(int i = 0; i < LETTERS; ++i) {
        counts[i] = 0;
        start_pos[i] = 0;
    }
    for(int i = 0; i < bwt.size(); ++i) {
        current = bwt.at(i);
        switch(current) {
            case 'A':
                counts[0]++;
                break;
            case 'C' :
                counts[1]++;
                break;
            case 'G' :
                counts[2]++;
                break;
            case 'T' :
                counts[3]++;
                break;
            default :
                initials.push_back(current);
        }
        for(int j = 0; j < LETTERS; ++j) {
            count_matrix.at(i).at(j) = counts[j];
        }   
    }
    for(int i = 0; i < LETTERS; ++i) {
        if(counts[i] != 0) {
            start_pos[i] = start;
            start += counts[i];
        }
    }
    for(int i = 0; i < LETTERS; ++i) {
        counter = counts[i];
        while(counter) {
            initials.push_back(int_to_char(i));
            counter--;
        }
    }
    // for(int i = 0; i < bwt.size(); ++i) {
    //     for(int j = 0; j < LETTERS; ++j) {
    //         cout << count_matrix.at(i).at(j) << ' ';
    //     }
    //     cout << '\n';
    // }
    // for(int i = 0; i < LETTERS; ++i) {
    //     cout << counts[i] << ' ' << start_pos[i] << '\n';
    // }
    current = bwt.at(0);
    int j = 0;
    while(current != '$') {
        text += current;
        j = start_pos[char_to_int(current)] + count_matrix.at(j).at(char_to_int(current)) - 1;
        current = bwt.at(j);
    }
    std::reverse(text.begin(), text.end());
    text += "$";

    return text;
}

int main() {
    string bwt;
    cin >> bwt;
    cout << InverseBWT(bwt) << endl;
    return 0;
}
