#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::string;
using std::vector;

const int LETTERS = 5;

int char_to_int(char x) {
    switch(x) {
        case '$' :
            return 0;
        case 'A' :
            return 1;
        case 'C' :
            return 2;
        case 'G' :
            return 3;
        case 'T' :
            return 4;
    }
}

vector<int> sort_characters(const string &text) {
    int count[LETTERS];
    vector<int> order(text.size());
    for(int i = 0; i < LETTERS; ++i) {
        count[i] = 0;
    }
    for(int i = 0; i < text.size(); ++i) {
        count[char_to_int(text.at(i))]++;
    }
    for(int i = 1; i < LETTERS; ++i) {
        count[i] += count[i - 1];
    }
    for(int i = text.size() - 1; i >= 0; i--) {
        char c = text.at(i);
        count[char_to_int(c)]--;
        order.at(count[char_to_int(c)]) = i;
    }

    return order;
}

vector<int> compute_char_classes(const string &text, vector<int> &order) {
    vector<int> eq_classes(text.size());
    eq_classes.at(order.at(0)) = 0;
    for(int i = 1; i < text.size(); ++i) {
        if(text.at(order.at(i)) != text.at(order.at(i - 1))) {
            eq_classes.at(order.at(i)) = eq_classes.at(order.at(i - 1)) + 1;
        }
        else {
            eq_classes.at(order.at(i)) = eq_classes.at(order.at(i - 1));
        }
    }

    return eq_classes;
}

vector<int> sort_doubled(const string &text, int l, vector<int> &order, vector<int> &eq_class) {
    vector<int> count(text.size(), 0);
    vector<int> new_order(text.size());
    for(int i = 0; i < text.size(); ++i) {
        count.at(eq_class.at(i))++;
    }
    for(int i = 1; i < text.size(); ++i) {
        count.at(i) += count.at(i - 1);
    }
    int start, cl;
    for(int i = text.size() - 1; i >= 0; i--) {
        start = (order.at(i) - l + text.size()) % text.size();
        cl = eq_class.at(start);
        count.at(cl)--;
        new_order.at(count.at(cl)) = start;
    }

    return new_order;
}

vector<int> update_classes(vector<int> &new_order, vector<int> &eq_class, int l) {
    int n = new_order.size();
    vector<int> new_class(n);
    new_class.at(new_order.at(0)) = 0;
    int cur, prev, mid, mid_prev;
    for(int i = 1; i < n; ++i) {
        cur = new_order.at(i);
        prev = new_order.at(i - 1);
        mid = (cur + l) % n;
        mid_prev = (prev + l) % n;
        if(eq_class.at(cur) != eq_class.at(prev) || eq_class.at(mid) != eq_class.at(mid_prev)) {
            new_class.at(cur) = new_class.at(prev) + 1; 
        }
        else {
            new_class.at(cur) = new_class.at(prev);
        } 
    }

    return new_class;
}

vector<int> BuildSuffixArray(const string& text) {
    vector<int> result, eq_class;
    result = sort_characters(text);
    eq_class = compute_char_classes(text, result);
    int l = 1;
    while(l < text.size()) {
        result = sort_doubled(text, l, result, eq_class);
        eq_class = update_classes(result, eq_class, l);
        l *= 2;
    }

    return result;
}

vector<int> match_positions(char current_symbol, const string &text, vector<int> &current_res, const vector<int> &suffix, int pos) {
    vector<int> result;
    for(int i = 0; i < current_res.size(); ++i) {
        if(text.at((current_res.at(i) - 1 + text.size()) % text.size()) == current_symbol) {
            result.push_back(current_res.at(i) - 1);
        } 
    }

    return result;
}

vector<int> FindOccurrences(const string& pattern, const string& text, const vector<int>& suffix_array) {
    vector<int> result;
    if(pattern.size() <= text.size()) {
        vector<int> count(LETTERS, 0);
        vector<int> starts(LETTERS, 0);
        int start = 1;
        for(int i = 0; i < text.size() - 1; ++i) {
            count.at(char_to_int(text.at(i)))++;
        }
        for(int i = 1; i < LETTERS; ++i) {
            if(count.at(i)) {
                starts.at(i) = start;
                start += count.at(i);
            }
        }
        int pos = pattern.size() - 1;
        char current_symbol = pattern.at(pos);
        if(starts.at(char_to_int(current_symbol))) {
            for(int i = starts.at(char_to_int(current_symbol)); i < starts.at(char_to_int(current_symbol)) + count.at(char_to_int(current_symbol)); ++i) {
                result.push_back(suffix_array.at(i));
            }
            pos--;
            while(pos >= 0 && !result.empty()) {
                current_symbol = pattern.at(pos);
                result = match_positions(current_symbol, text, result, suffix_array, pos);
                pos--;
            }
        }
    }

    return result;
}

int main() {
    char buffer[100001];
    scanf("%s", buffer);
    string text = buffer;
    text += '$';
    vector<int> suffix_array = BuildSuffixArray(text);
    int pattern_count;
    scanf("%d", &pattern_count);
    vector<bool> occurs(text.length(), false);
    for (int pattern_index = 0; pattern_index < pattern_count; ++pattern_index) {
        scanf("%s", buffer);
        string pattern = buffer;
        vector<int> occurrences = FindOccurrences(pattern, text, suffix_array);
        for (int j = 0; j < occurrences.size(); ++j) {
            occurs[occurrences[j]] = true;
        }
    }
    for (int i = 0; i < occurs.size(); ++i) {
        if (occurs[i]) {
            printf("%d ", i);
        }
    }
    printf("\n");
    return 0;
}
