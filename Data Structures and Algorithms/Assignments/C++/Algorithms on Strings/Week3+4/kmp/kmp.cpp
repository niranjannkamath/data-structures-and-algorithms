#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;

// Find all occurrences of the pattern in the text and return a
// vector with all positions in the text (starting from 0) where 
// the pattern starts in the text.
vector<int> compute_prefix_function(const string &text) {
    vector<int> result(text.size(), 0);
    int border = 0;
    for(int i = 1; i < text.size(); ++i) {
        while(border > 0 && text.at(i) != text.at(border)) {
            border = result.at(border - 1);
        }
        if(text.at(i) == text.at(border)) {
            border += 1;
        }
        else {
            border = 0;
        }
        result.at(i) = border;
    }
    return result;
}
vector<int> find_pattern(const string& pattern, const string& text) {
    vector<int> result, prefixes;
    string long_string = pattern + "$" + text;
    prefixes = compute_prefix_function(long_string);
    for(int i = pattern.size() + 1; i < long_string.size(); ++i) {
        if(prefixes.at(i) == pattern.size()) {
            result.push_back(i - 2 * pattern.size());
        }
    }

    return result;
}

int main() {
    string pattern, text;
    cin >> pattern;
    cin >> text;
    vector<int> result = find_pattern(pattern, text);
    for (int i = 0; i < result.size(); ++i) {
        cout << result[i] << ' ';
    }
    cout << endl;
    return 0;
}
