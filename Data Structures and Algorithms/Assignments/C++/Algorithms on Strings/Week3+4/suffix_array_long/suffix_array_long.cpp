#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <utility>

using std::cin;
using std::cout;
using std::endl;
using std::make_pair;
using std::pair;
using std::string;
using std::vector;

const int LETTERS = 5;

int char_to_int(char x) {
    switch(x) {
        case '$' :
            return 0;
        case 'A' :
            return 1;
        case 'C' :
            return 2;
        case 'G' :
            return 3;
        case 'T' :
            return 4;
    }
}

vector<int> sort_characters(const string &text) {
    int count[LETTERS];
    vector<int> order(text.size());
    for(int i = 0; i < LETTERS; ++i) {
        count[i] = 0;
    }
    for(int i = 0; i < text.size(); ++i) {
        count[char_to_int(text.at(i))]++;
    }
    for(int i = 1; i < LETTERS; ++i) {
        count[i] += count[i - 1];
    }
    for(int i = text.size() - 1; i >= 0; i--) {
        char c = text.at(i);
        count[char_to_int(c)]--;
        order.at(count[char_to_int(c)]) = i;
    }

    return order;
}

vector<int> compute_char_classes(const string &text, vector<int> &order) {
    vector<int> eq_classes(text.size());
    eq_classes.at(order.at(0)) = 0;
    for(int i = 1; i < text.size(); ++i) {
        if(text.at(order.at(i)) != text.at(order.at(i - 1))) {
            eq_classes.at(order.at(i)) = eq_classes.at(order.at(i - 1)) + 1;
        }
        else {
            eq_classes.at(order.at(i)) = eq_classes.at(order.at(i - 1));
        }
    }

    return eq_classes;
}

vector<int> sort_doubled(const string &text, int l, vector<int> &order, vector<int> &eq_class) {
    vector<int> count(text.size(), 0);
    vector<int> new_order(text.size());
    for(int i = 0; i < text.size(); ++i) {
        count.at(eq_class.at(i))++;
    }
    for(int i = 1; i < text.size(); ++i) {
        count.at(i) += count.at(i - 1);
    }
    int start, cl;
    for(int i = text.size() - 1; i >= 0; i--) {
        start = (order.at(i) - l + text.size()) % text.size();
        cl = eq_class.at(start);
        count.at(cl)--;
        new_order.at(count.at(cl)) = start;
    }

    return new_order;
}

vector<int> update_classes(vector<int> &new_order, vector<int> &eq_class, int l) {
    int n = new_order.size();
    vector<int> new_class(n);
    new_class.at(new_order.at(0)) = 0;
    int cur, prev, mid, mid_prev;
    for(int i = 1; i < n; ++i) {
        cur = new_order.at(i);
        prev = new_order.at(i - 1);
        mid = (cur + l) % n;
        mid_prev = (prev + l) % n;
        if(eq_class.at(cur) != eq_class.at(prev) || eq_class.at(mid) != eq_class.at(mid_prev)) {
            new_class.at(cur) = new_class.at(prev) + 1; 
        }
        else {
            new_class.at(cur) = new_class.at(prev);
        } 
    }

    return new_class;
}

// Build suffix array of the string text and
// return a vector result of the same length as the text
// such that the value result[i] is the index (0-based)
// in text where the i-th lexicographically smallest
// suffix of text starts.
vector<int> BuildSuffixArray(const string& text) {
    vector<int> result, eq_class;
    result = sort_characters(text);
    eq_class = compute_char_classes(text, result);
    int l = 1;
    while(l < text.size()) {
        result = sort_doubled(text, l, result, eq_class);
        eq_class = update_classes(result, eq_class, l);
        l *= 2;
    }

    return result;
}

int main() {
    string text;
    cin >> text;
    
    vector<int> suffix_array = BuildSuffixArray(text);
    for (int i = 0; i < suffix_array.size(); ++i) {
        cout << suffix_array[i] << ' ';
    }
    cout << endl;
    return 0;
}
