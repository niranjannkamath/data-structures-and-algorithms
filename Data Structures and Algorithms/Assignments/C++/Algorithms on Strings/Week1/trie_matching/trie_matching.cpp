#include <algorithm>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std;

typedef map<char, int> edges;
typedef vector<edges> trie;

trie build_trie(const vector<string> &patterns) {
    trie t;
    string pattern;
    pair<char,int> p; 
    edges e;
    t.push_back(e);
    auto it = e.begin();
    int current_node, edge_num = 1;
    char current_symbol;
    for(int i = 0; i < patterns.size(); ++i) {
        pattern = patterns.at(i);
        current_node = 0;
        for(int j = 0; j < pattern.size(); ++j) {
            e = t.at(current_node);
            it = e.find(pattern.at(j));
            if(it == e.end()) {
                while(j < pattern.size()) {
                    current_symbol = pattern.at(j);
                    p.first = current_symbol;
                    p.second = edge_num++;
                    e.insert(p);
                    t.at(current_node) = e;
                    e.clear();
                    t.push_back(e);
                    if(j != pattern.size() - 1) {
                        current_node = t.size() - 1;
                    }
                    j++;
                }
                break;
            }
            current_node = it->second;
        }
    }
    
    return t;
}

vector <int> solve (const string &text, int n, const vector <string> &patterns) {
	vector <int> result;
    trie t = build_trie(patterns);
    int current_node, len;
    char current_symbol;
    for(int i = 0; i < text.size(); ++i) {
        current_node = 0, len = 0;
        edges e = t.at(0);
        auto it = e.find(text.at(i + len));
        while(it != e.end()) {
            current_node = it->second;
            e = t.at(current_node);
            if(e.empty() || (i + len == t.size() - 1 && e.empty())) {
                result.push_back(i);
                break;
            }
            else if(i + len == text.size() - 1) {
                break;
            }
            else {
                len++;
                it = e.find(text.at(i + len));
            }
        }
    }
	return result;
}

int main (void) {
	string text;
	cin >> text;

	int n;
	cin >> n;

	vector <string> patterns (n);
	for (int i = 0; i < n; i++) {
		cin >> patterns[i];
	}

	vector <int> ans;
	ans = solve (text, n, patterns);

	for (int i = 0; i < (int) ans.size (); i++) {
		cout << ans[i];
		if (i + 1 < (int) ans.size ()) {
			cout << " ";
		}
		else {
			cout << endl;
		}
	}

	return 0;
}
