#include <string>
#include <iostream>
#include <vector>
#include <map>

using std::map;
using std::pair;
using std::vector;
using std::string;

typedef map<char, int> edges;
typedef vector<edges> trie;

trie build_trie(vector<string> &patterns) {
    trie t;
    string pattern;
    pair<char,int> p; 
    edges e;
    t.push_back(e);
    auto it = e.begin();
    int current_node, edge_num = 1;
    char current_symbol;
    for(int i = 0; i < patterns.size(); ++i) {
        pattern = patterns.at(i);
        current_node = 0;
        for(int j = 0; j < pattern.size(); ++j) {
            e = t.at(current_node);
            it = e.find(pattern.at(j));
            if(it == e.end()) {
                while(j < pattern.size()) {
                    current_symbol = pattern.at(j);
                    p.first = current_symbol;
                    p.second = edge_num++;
                    e.insert(p);
                    t.at(current_node) = e;
                    e.clear();
                    if(j != pattern.size() - 1) {
                        current_node = t.size() - 1;
                    }
                    j++;
                }
                break;
            }
            current_node = it->second;
        }
    }
    
    return t;
}

int main() {
    size_t n;
    std::cin >> n;
    vector<string> patterns;
    for (size_t i = 0; i < n; i++) {
        string s;
        std::cin >> s;
        patterns.push_back(s);
    }

    trie t = build_trie(patterns);
    for (size_t i = 0; i < t.size(); ++i) {
        for (const auto & j : t[i]) {
        std::cout << i << "->" << j.second << ":" << j.first << "\n";
        }
    }

    return 0;
}