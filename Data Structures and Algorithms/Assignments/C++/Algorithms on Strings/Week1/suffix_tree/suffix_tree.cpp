#include <iostream>
#include <map>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::string;
using std::vector;
using std::pair;

typedef std::map<char, int>::iterator map_it;

class Edge {
public:
    map<char, int> edge;
    int length;
    int pos;
    map_it find(char symbol) {
        return edge.find(symbol);
    }
    map_it begin() {
        return edge.begin();
    }
};

// Build a suffix tree of the string text and return a vector
// with all of the labels of its edges (the corresponding 
// substrings of the text) in any order.
vector<string> ComputeSuffixTreeEdges(const string& text) {
    vector<string> result;
    vector<Edge> edges;
    Edge e1, e2, e3;
    pair<char, int> p;
    int current_node, current_pos, current_len, current_pointer, edge_num = 1;
    char current_symbol;
    e1.pos = -1;
    e1.length = 0;
    map_it it = e1.begin();
    edges.push_back(e1);
    for(size_t i = 0; i < text.size(); ++i) {
        string sub = text.substr(i);
        current_node = 0, current_pos = 0, current_len = sub.size();
        current_symbol = sub.at(current_pos);
        e1 = edges.at(current_node);
        it = e1.find(current_symbol);
        while(current_len) {
            if(it == e1.edge.end()) {
                p.first = current_symbol;
                p.second = edge_num++;
                edges.at(current_node).edge.insert(p);
                e1.pos = i + current_pos;
                e1.length = current_len;
                edges.push_back(e1);
                break;
            }
            else {
                current_node = it->second;
                e1 = edges.at(current_node);
                current_pointer = 0;
                while(current_pointer <= e1.length - 1 && text.at(e1.pos + current_pointer) == current_symbol) {
                    current_pos++;
                    current_pointer++;
                    current_len--;
                    current_symbol = sub.at(current_pos);
                }
                if(current_pointer > e1.length - 1 && current_len) {
                    it = e1.find(current_symbol);
                }
                else if(text.at(e1.pos + current_pointer) != current_symbol) {
                    p.first = current_symbol;
                    p.second = edge_num++;
                    e3.edge = e1.edge;
                    e3.pos = e1.pos + current_pointer;
                    e3.length = e1.length - current_pointer;
                    e2.pos = i + current_pos;
                    e2.length = current_len;
                    e1.edge.clear();
                    e1.edge.insert(p);
                    p.first = text.at(e1.pos + current_pointer);
                    p.second = edge_num++;
                    e1.edge.insert(p);
                    e1.length = current_pointer;
                    edges.push_back(e2);
                    edges.push_back(e3);
                    edges.at(current_node) = e1;
                    break;
                }
            }
        }
    }
    for(int i = 1; i < edges.size(); ++i) {
        result.push_back(text.substr(edges.at(i).pos, edges.at(i).length));
    }
    return result;
}

int main() {
    string text;
    cin >> text;
    vector<string> edges = ComputeSuffixTreeEdges(text);
    for (int i = 0; i < edges.size(); ++i) {
        cout << edges[i] << endl;
    }
    return 0;
}