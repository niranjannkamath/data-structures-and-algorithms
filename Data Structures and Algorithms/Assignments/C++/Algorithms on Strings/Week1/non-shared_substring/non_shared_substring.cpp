#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <queue>

using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::string;
using std::vector;
using std::pair;
using std::priority_queue;

typedef std::map<char, int>::iterator map_it;

class Edge {
public:
    map<char, int> edge;
    int length;
    int pos, parent, is_shared = 0;
    map_it find(char symbol) {
        return edge.find(symbol);
    }
    map_it begin() {
        return edge.begin();
    }
};

vector<Edge> ComputeSuffixTreeEdges(const string& text) {
    vector<Edge> edges;
    Edge e1, e2, e3;
    pair<char, int> p;
    int current_node, current_pos, current_len, current_pointer, edge_num = 1;
    char current_symbol;
    e1.pos = -1;
    e1.length = 0;
    e1.parent = -1;
    e1.is_shared = 0;
    map_it it = e1.begin();
    edges.push_back(e1);
    for(size_t i = 0; i < text.size(); ++i) {
        string sub = text.substr(i);
        current_node = 0, current_pos = 0, current_len = sub.size();
        current_symbol = sub.at(current_pos);
        e1 = edges.at(current_node);
        it = e1.find(current_symbol);
        while(current_len) {
            if(it == e1.edge.end()) {
                p.first = current_symbol;
                p.second = edge_num++;
                edges.at(current_node).edge.insert(p);
                e1.edge.clear();
                e1.pos = i + current_pos;
                e1.length = current_len;
                e1.parent = current_node;
                e1.is_shared = 0;
                edges.push_back(e1);
                break;
            }
            else {
                current_node = it->second;
                e1 = edges.at(current_node);
                current_pointer = 0;
                while(current_pointer <= e1.length - 1 && text.at(e1.pos + current_pointer) == current_symbol) {
                    current_pos++;
                    current_pointer++;
                    current_len--;
                    current_symbol = sub.at(current_pos);
                }
                if(current_pointer > e1.length - 1 && current_len) {
                    it = e1.find(current_symbol);
                }
                else if(text.at(e1.pos + current_pointer) != current_symbol) {
                    p.first = current_symbol;
                    p.second = edge_num++;
                    e3.edge = e1.edge;
                    e3.parent = current_node;
                    e3.is_shared = 0;
                    for(map_it it = e3.edge.begin(); it != e3.edge.end(); ++it) {
                        edges.at(it->second).parent = edge_num;
                    }
                    e3.pos = e1.pos + current_pointer;
                    e3.length = e1.length - current_pointer;
                    e2.pos = i + current_pos;
                    e2.length = current_len;
                    e2.parent = current_node;
                    e2.is_shared = 0;
                    e1.edge.clear();
                    e1.edge.insert(p);
                    p.first = text.at(e1.pos + current_pointer);
                    p.second = edge_num++;
                    e1.edge.insert(p);
                    e1.length = current_pointer;
                    e1.is_shared = 0;
                    edges.push_back(e2);
                    edges.push_back(e3);
                    edges.at(current_node) = e1;
                    break;
                }
            }
        }
    }

    return edges;
}

class Compare {
public:
    bool operator()(Edge &e1, Edge &e2) {
        return (e1.length < e2.length);
    }
};

bool has_non_shared_pos(Edge e, vector<Edge> edges, int max) {
    if(e.pos > max) {
        return true;
    }
    bool non_shared = false;
    for(map_it it = e.edge.begin(); it != e.edge.end(); ++it) {
        non_shared = has_non_shared_pos(edges.at(it->second), edges, max);
        if(non_shared) {
            break;
        }
    }

    return non_shared;
}

string solve(string p, string q, string &result) {
    string combined = p + "#" + q + "$";
    vector<Edge> edges = ComputeSuffixTreeEdges(combined);
    vector<int> s1_leaves;

    int parent, length, pos, current, min_length = p.size(), min_index = 0;

    for(int i = 0; i < edges.size(); ++i) {
        parent = edges.at(i).parent;
        if(edges.at(i).edge.empty() && edges.at(i).pos > p.size()) {
            while(parent != -1 && edges.at(parent).is_shared == 0) {
                edges.at(parent).is_shared = 1;
                parent = edges.at(parent).parent;
            }
        }
        else if(edges.at(i).edge.empty()){
            s1_leaves.push_back(i);
        }
    }
    // for(int i = 0; i < edges.size(); ++i) {
    //     cout << edges.at(i).is_shared << ' ';
    // }
    // cout << '\n';
    for(int i = 0; i < s1_leaves.size(); ++i) {
        length = 0;
        parent = edges.at(s1_leaves.at(i)).parent;
        current = s1_leaves.at(i);
        if(edges.at(s1_leaves.at(i)).edge.empty() && edges.at(s1_leaves.at(i)).pos < p.size()) {
            // for(map_it it = edges.at(parent).edge.begin(); it != edges.at(parent).edge.end(); ++it) {
            //     if(edges.at(it->second)..is_shared) {
            //         length = 1;
            //         break;
            //     }
            // }
            if(edges.at(parent).is_shared) {
                //cout << 2;
                length = 1;
            }
            while(parent != 0) {
                length += edges.at(parent).length;
                current = parent;
                parent = edges.at(parent).parent;
            }
            if(length <= min_length) {
                min_length = length;
                pos = edges.at(s1_leaves.at(i)).pos;
                //cout << i << ' ' << length << ' ' << pos << '\n';
                min_index = s1_leaves.at(i);
            }
        }
        else if(edges.at(s1_leaves.at(i)).pos == p.size()) {
            if(parent == 0 || edges.at(parent).is_shared) {
                continue;
            }
            while(parent != 0) {
                length += edges.at(parent).length;
                current = parent;
                parent = edges.at(parent).parent;
            }
            if(length <= min_length) {
                min_length = length;
                pos = edges.at(s1_leaves.at(i)).pos;
                min_index = s1_leaves.at(i);
            }
        }
    }
    parent = edges.at(min_index).parent;
    int current_length = min_length;
    if(edges.at(min_index).length > p.size() + 2) {
        current_length--;
    }
    while(current_length >= 0 && parent != 0) {
        current_length -= edges.at(parent).length;
        pos -= edges.at(parent).length;
        parent = edges.at(parent).parent;
    }
    result = p.substr(pos, min_length);

    return result;
}

int main () {
	string p;
	cin >> p;
	string q;
	cin >> q;
    string result = "";

	string ans = solve(p, q, result);

	cout << ans << endl;

	return 0;
}